/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains additional information about a status
 * change detection.
 * <p>
 * Abbreviation: QOS
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class StatusAndChangeDetection implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 5808361934565104754L;

	/**
	 * The status.
	 * <p>
	 * Abbreviation: ST
	 */
	private ShortValue status;

	/**
	 * The change detection.
	 * <p>
	 * Abbreviation: CD
	 */
	private ShortValue changeDetection;

	/**
	 * Return the status.
	 * <p>
	 * Abbreviation: ST
	 * 
	 * @return A {@link ShortValue}.
	 */
	public ShortValue getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * <p>
	 * Abbreviation: ST
	 * 
	 * @param status A {@link ShortValue}.
	 */
	public void setStatus(ShortValue status) {
		this.status = status;
	}

	/**
	 * Returns the change detection.
	 * <p>
	 * Abbreviation: CD
	 * 
	 * @return A {@link ShortValue}.
	 */
	public ShortValue getChangeDetection() {
		return changeDetection;
	}

	/**
	 * Sets the change detection.
	 * <p>
	 * Abbreviation: CD
	 * 
	 * @param changeDetection A {@link ShortValue}.
	 */
	public void setChangeDetection(ShortValue changeDetection) {
		this.changeDetection = changeDetection;
	}

	/**
	 * Empty default constructor.
	 */
	public StatusAndChangeDetection() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param status          The status.
	 *                        <p>
	 *                        Abbreviation: ST
	 * @param changeDetection The change detection.
	 *                        <p>
	 *                        Abbreviation: CD
	 */
	public StatusAndChangeDetection(ShortValue status, ShortValue changeDetection) {
		this.status = status;
		this.changeDetection = changeDetection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((changeDetection == null) ? 0 : changeDetection.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StatusAndChangeDetection other = (StatusAndChangeDetection) obj;
		if (changeDetection == null) {
			if (other.changeDetection != null) {
				return false;
			}
		} else if (!changeDetection.equals(other.changeDetection)) {
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		status = new ShortValue();
		byte[] remainingBytes = status.fromBytes(bytes);
		changeDetection = new ShortValue();
		remainingBytes = changeDetection.fromBytes(remainingBytes);
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] statusBytes = status.toBytes();
		byte[] changeDetectionBytes = changeDetection.toBytes();
		return ArrayUtils.addAll(statusBytes, changeDetectionBytes);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return ShortValue.getEncodedSize() * 2;
	}

}