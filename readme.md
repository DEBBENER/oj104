# OpenJava 104 (OJ104) Library #

OJ104 is an open-source Java implementation of the IEC 60870-5-104 protocol. It provides a full data model as well as serialization from and deserialization to binary and JSON representation.
In addition, a communication layer is included to handle the communication via TCP.

## Structure

The main structure of OJ104 is as follows:

* ICommunicationHandler:
Interface for services that handle the sending and receiving of 104 messages, either as server or as client. 
The main objective of a communication handler is to act as a TCP server or client, sending 104 messages and transferring received ones to an APDU handler.
Example server and client communication handlers are included in OJ104.

* IAPDUHandler:
Interface for services that handle APDUs. An APDU handler has two main objectives:
1. Handling of incoming APDUs from a communication handler. This handling includes the handling of send and receive sequence numbers and timers as well as the sending of responses via a communication handler.
2. Sending of unnumbered control functions to start or stop the data transfer.
A default APDU handler is included in OJ104.

* IASDUHandler:
Interface for services that handle ASDUs. An ASDU handler has the objective to provide an ASDU as response to an incoming ASDU from an APDU handler. 
It is also possible to not respond with an ASDU. This is what a default implementation, that is included in OJ104, does.

* Data model:
The complete structure from APDUs down to types of information elements and time tags are implemented in the data model of OJ104. Each element can be deserialized from bytes and serialized to bytes.
The (de) serialization includes all contained elements. For example, the serialization of an APDU serializes its whole content.
In addition, the toString methods of all data model classes is implemented in a way, that it returns a JSON representation of the object. 
A JSON parser, integrated in OJ104, can be used to deserialize a JSON object to an OJ104 data model object.

## Development with OJ104

In case you want to add OJ104 as a library to your software, you typically want to add new implementations:

* ICommunicationHandler:
The example server and client communication handlers are implemented just to demonstrate the functionalities of OJ104. 
The server listens to console inputs of the user and sends random ASDUs of a type typed in by the user.
So, a server and/or client, depending on your needs, should be added for the use case of OJ104.

* IAPDUHandler:
In most cases, the StandardAPDUHandler in OJ104 should be sufficient, because it implements all features of the protocol incl. timers.

* IASDUHandler:
The DummyASDU handler should not be used, if you want to do something with the received ASDUs. 
It is recommended to implement an own ASDU handler. If you don't want to respond with an ASDU, it is recommended to start a new thread in the ASDU handler that processes the ASDU and to return Optional.empty immediately.

## Build

Maven can be used to build a JAR:

```console
mvn -f de.uniol.inf.ei.oj104/pom.xml clean package
```

## Running example

OJ104 contains two different applications:
1. Client Application
2. Server Application

Use the following command in the directory of the JAR to run the server:

```console
java -cp de.uniol.inf.ei.oj104/target/oj104-0.0.7-jar-with-dependencies.jar de.uniol.inf.ei.oj104.application.ServerApplication "port"(optional)
```

Use the version of your JAR instead of 0.0.7.
You can also add a port as argument. If not, the default (2404) is used.

When a client is connected and the handshake is done, an ASDU with random values can be sent by entering the ASDU ID.
Entering "stop" stops the server.

Use the following command in the directory of the JAR to run the client:

```console
java -cp de.uniol.inf.ei.oj104/target/oj104-0.0.7-jar-with-dependencies.jar de.uniol.inf.ei.oj104.application.ClientApplication "host" "port"(optional)
```

Use the version of your JAR instead of 0.0.7.
You need to add the host as argument and you can also add a port as argument. If not, the default (2404) is used.
Entering "stop" stops the client.

## Example JSON output

The following code is the JSON serialization of an example APDU:
```json
{
  "apci" : {
    "length" : 14,
    "controlField" : {
      "sendSequenceNumber" : 0,
      "receiveSequenceNumber" : 0
    }
  },
  "asdu" : {
    "dataUnitIdentifier" : {
      "dataUnitType" : {
        "typeIdentification" : {
          "id" : 1,
          "description" : "single-point information",
          "code" : "M_SP_NA_1"
        },
        "structureQualifier" : "SINGLE",
        "numberOfInformationElements" : 1
      },
      "causeOfTransmission" : {
        "id" : 2,
        "description" : "background scan"
      },
      "confirm" : "POSITIVE",
      "test" : true,
      "originatorAddress" : 0,
      "commonAddressOfASDU" : {
        "value" : 0
      }
    },
    "informationObjects" : [ {
      "@type" : "SingleInformationObject",
      "informationElementClasses" : [ "de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation" ],
      "timeTagClass" : null,
      "informationElements" : [ {
        "@type" : "SinglePointInformation",
        "on" : false,
        "invalid" : true,
        "notTopical" : false,
        "blocked" : false,
        "substituted" : false
      } ],
      "timeTag" : null,
      "informationObjectAddress" : 0
    } ]
  }
}
```
