/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.application;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uniol.inf.ei.oj104.communication.DummyASDUHandler;
import de.uniol.inf.ei.oj104.communication.IAPDUHandler;
import de.uniol.inf.ei.oj104.communication.IASDUHandler;
import de.uniol.inf.ei.oj104.communication.ICommunicationHandler;
import de.uniol.inf.ei.oj104.communication.StandardAPDUHandler;
import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;
import de.uniol.inf.ei.oj104.util.SystemProperties;

/**
 * Client station (controlling station) as an {@link ICommunicationHandler} that
 * handles the sending and receiving of 104 messages. An
 * {@link ICommunicationHandler} is part of the communication architecure of
 * OJ104. The other parts are an {@link IAPDUHandler} and an
 * {@link IASDUHandler}. The main objective of a client
 * {@link ICommunicationHandler} is to send 104 messages and transfer received
 * ones to an {@link IAPDUHandler}.
 *
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ClientApplication implements ICommunicationHandler {

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = LoggerFactory.getLogger(ClientApplication.class);

	/**
	 * The default port to use.
	 */
	private static final int defaultPort = Integer
			.parseInt(SystemProperties.getProperties().getProperty("port.default", "2404"));

	/**
	 * Starts a TCP client connecting to a host (args[0]) and a port (args[1] or
	 * default port defined in {@link SystemProperties}).
	 */
	public static void main(String args[]) throws Exception {
		int port = defaultPort;
		if (args == null || args.length == 0) {
			logger.error("ClientApplication must have at least one argument: the server ip!");
			return;
		}
		String host = args[0];
		if (args.length > 1) {
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				logger.error("Second argument of ClientApplication must be a port number!", e);
				return;
			}
		}
		new ClientApplication().start(host, port);
	}

	/**
	 * An {@link IASDUHandler} has the objective to provide an {@link ASDU} as
	 * response to an incoming {@link ASDU} from an {@link IAPDUHandler}. It is also
	 * possible to not respond with an {@link ASDU} ({@link Optional#empty()}).
	 */
	private IASDUHandler asduHandler = new DummyASDUHandler();

	/**
	 * An {@link IAPDUHandler} has two main objectives:
	 * <ol>
	 * <li>Handling of incoming {@link APDU}s from an {@link ICommunicationHandler}.
	 * This handling includes the handling of send and reseice sequence numbers and
	 * timers as well as the sending of responses via an
	 * {@link ICommunicationHandler}.</li>
	 * <li>Sending of {@link UnnumberedControlFunction}s to start or stop the data
	 * transfer.</li>
	 * </ol>
	 */
	private IAPDUHandler apduHandler = new StandardAPDUHandler();

	/**
	 * The used client socket.
	 */
	private Socket socket;

	/**
	 * The lock prevents the application to end. Unlock can be caused by a user
	 * command or an exception while reading from socket.
	 */
	private Object lock = new Object();

	/**
	 * Thread to listen to console commands from the user.
	 */
	private Thread commandListener = new Thread(new Runnable() {

		@Override
		public void run() {
			BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

			// listen to console for stop command
			while (true) {
				try {
					System.out.println("Type in 'stop' to stop!");
					String input = stdIn.readLine();
					if (input != null && input.toLowerCase().equals("stop")) {
						break;
					}
				} catch (IOException e) {
					logger.error("Error while reading from console!", e);
					closeConnection();
				}
			}

			// stop data transfer
			try {
				apduHandler.stopDataTransfer();
			} catch (IEC608705104ProtocolException | IOException e) {
				logger.error("Error while stopping data transfer!", e);
				closeConnection();
			}

			// stop threads
			apduListener.interrupt();
			synchronized (lock) {
				lock.notifyAll();
			}
		}

	}, "104 Client Command Listener");

	/**
	 * Thread to listen to {@link APDU}s on the socket.
	 */
	private Thread apduListener = new Thread(new Runnable() {

		@Override
		public void run() {
			// listening for APDUs
			while (true) {
				try {
					receive();
				} catch (IEC608705104ProtocolException e) {
					logger.error("Error while reading from socket!", e);
				} catch (IOException e) {
					break;
				}
			}

			// stop threads
			commandListener.interrupt();
			synchronized (lock) {
				lock.notifyAll();
			}
		}

	}, "APDU Listener");

	/**
	 * Starts a client connection, sends a startDT message, starts the
	 * {@link #commandListener} and the {@link #apduListener}.
	 * 
	 * @param host The host of the server.
	 * @param port The port on the server host.
	 */
	private void start(String host, int port) {
		apduHandler.setASDUHandler(asduHandler);
		apduHandler.setCommunicationHandler(this);
		((StandardAPDUHandler) apduHandler).setLogger(logger);

		try {
			socket = new Socket(host, port);
			logger.info("Client started, sending to host:port {}:{}", host, port);

			// start data transfer
			try {
				apduHandler.startDataTransfer();
			} catch (IEC608705104ProtocolException e) {
				logger.error("Error while starting data transfer!", e);
				closeConnection();
			}

			// start listener for APDUs and commands.
			apduListener.start();
			commandListener.start();

			try {
				synchronized (lock) {
					lock.wait();
				}
			} catch (InterruptedException e) {
				logger.error("Error while waiting for APDU listener to be interrupted", e);
			}
			closeConnection();
			logger.info("Client stopped");
		} catch (IOException e) {
			closeConnection();
		}
	}

	@Override
	public void send(APDU apdu) throws IOException, IEC608705104ProtocolException {
		byte[] bytes = apdu.toBytes();
		synchronized (socket) {
			DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());
			dOut.write(bytes);
		}
		logger.debug("Sent (APDU): {}", apdu);
		logger.trace("Sent (bytes): {}", bytes);
	}

	@Override
	public void closeConnection() {
		apduHandler.stopAllThreads();
		try {
			synchronized (socket) {
				if (socket != null) {
					socket.close();
				}
			}
		} catch (IOException e) {
			logger.error("Error while closimng socket!", e);
		}
	}

	/**
	 * Waiting for and handling of incoming messages on the web socket. Calling
	 * {@link APDU#fromBytes(byte[])} and {@link IAPDUHandler#handleAPDU(APDU)}.
	 */
	public void receive() throws IOException, IEC608705104ProtocolException {
		DataInputStream in;

		synchronized (socket) {
			in = new DataInputStream(socket.getInputStream());
		}

		byte[] header = new byte[2];
		int headerLength = in.read(header, 0, 2);
		if (headerLength != 2) {
			return;
		}

		int restLength = header[1] & 0xff;
		byte[] rest = new byte[restLength];
		int length = in.read(rest, 0, restLength);
		if (restLength != length) {
			throw new IEC608705104ProtocolException(
					"Received " + (length + 2) + " bytes but expected " + (restLength + 2) + " bytes!");
		}

		byte[] bytes = ArrayUtils.addAll(header, rest);
		logger.trace("Received (bytes): {}", bytes);

		APDU apdu = new APDU();
		apdu.fromBytes(bytes);
		logger.debug("Received (APDU): {}", apdu);

		apduHandler.handleAPDU(apdu);
	}

}