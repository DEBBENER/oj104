/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

/**
 * The confirm indicates a positive or negative confirmation of activation
 * requested by the primary application function.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public enum Confirm implements Serializable {

	/**
	 * A positive confirm with code 0.
	 */
	POSITIVE(0),

	/**
	 * A negative confirm with code 1.
	 */
	NEGATIVE(1);

	/**
	 * The code of the confirm: 0 = positive, 1 = negative.
	 */
	private int code;

	/**
	 * Return the code of the confirm.
	 * 
	 * @return An integer: 0 = positive, 1 = negative.
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param code The code of the confirm: 0 = positive, 1 = negative.
	 */
	private Confirm(int code) {
		this.code = code;
	}

	/**
	 * Retrieve an enum entry.
	 * 
	 * @param code The code of the confirm: 0 = positive, 1 = negative.
	 * @return An optional of the {@link Confirm} or {@link Optional#empty()}, if
	 *         there is none for the code.
	 */
	public static Optional<Confirm> getConfirm(int code) {
		return Arrays.asList(values()).stream().filter(qualifier -> qualifier.code == code).findAny();
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}