/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.communication;

import java.io.IOException;

import de.uniol.inf.ei.oj104.application.ClientApplication;
import de.uniol.inf.ei.oj104.application.ServerApplication;
import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APDU;

/**
 * Interface for services that handle the sending and receiving of 104 messages,
 * either as server or as client. An {@link ICommunicationHandler} is part of
 * the communication architecure of OJ104. The other parts are an
 * {@link IAPDUHandler} and an {@link IASDUHandler}. The main objective of an
 * {@link ICommunicationHandler} is to act as a TCP server or client, sending
 * 104 messages and transferring received ones to an {@link IAPDUHandler}.
 * 
 * Default implementations: {@link ClientApplication},
 * {@link ServerApplication}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public interface ICommunicationHandler {

	/**
	 * Sends a given {@link APDU} via TCP.
	 */
	public void send(APDU apdu) throws IOException, IEC608705104ProtocolException;

	/**
	 * Closes the TCP connection.
	 */
	public void closeConnection();

}