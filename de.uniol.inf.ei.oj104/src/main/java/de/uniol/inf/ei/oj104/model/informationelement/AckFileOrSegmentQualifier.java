/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains the acknowledgement of a file or segment.
 * <p>
 * Abbreviation: AFQ
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "notUsed", "posAckOfFile", "negAckOfFile", "posAckOfSection", "negAckOfSection",
		"inCompatibleRange", "inPrivateRange" })
public class AckFileOrSegmentQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 7038452938759001084L;

	/**
	 * The ack value:
	 * <ul>
	 * <li>0 = not used</li>
	 * <li>1 = positive ack of file</li>
	 * <li>2 = negative ack of file</li>
	 * <li>3 = positive ack of section</li>
	 * <li>4 = negative ack of section</li>
	 * <li>5-10 = in compatible range</li>
	 * <li>11-15 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * File errors can be:
	 * <ul>
	 * <li>default</li>
	 * <li>not enough memory</li>
	 * <li>checksum failed</li>
	 * <li>unexpected communication service</li>
	 * <li>unexpected name of file</li>
	 * <li>unexpected name of section</li>
	 * <li>in compatible range</li>
	 * <li>in private range</li>
	 * </ul>
	 */
	private FileError error;

	/**
	 * Returns the ack value.
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = not used</li>
	 *         <li>1 = positive ack of file</li>
	 *         <li>2 = negative ack of file</li>
	 *         <li>3 = positive ack of section</li>
	 *         <li>4 = negative ack of section</li>
	 *         <li>5-10 = in compatible range</li>
	 *         <li>11-15 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the ack value.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = positive ack of file</li>
	 *              <li>2 = negative ack of file</li>
	 *              <li>3 = positive ack of section</li>
	 *              <li>4 = negative ack of section</li>
	 *              <li>5-10 = in compatible range</li>
	 *              <li>11-15 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the ack is "not used".
	 * 
	 * @return {@link #getValue()} == 0
	 */
	public boolean isNotUsed() {
		return value == 0;
	}

	/**
	 * Checks, whether the ack is "positive ack of file".
	 * 
	 * @return {@link #getValue()} == 1
	 */
	public boolean isPosAckOfFile() {
		return value == 1;
	}

	/**
	 * Checks, whether the ack is "negative ack of file".
	 * 
	 * @return {@link #getValue()} == 2
	 */
	public boolean isNegAckOfFile() {
		return value == 2;
	}

	/**
	 * Checks, whether the ack is "positive ack of section".
	 * 
	 * @return {@link #getValue()} == 3
	 */
	public boolean isPosAckOfSection() {
		return value == 3;
	}

	/**
	 * Checks, whether the ack is "negative ack of section".
	 * 
	 * @return {@link #getValue()} == 4
	 */
	public boolean isNegAckOfSection() {
		return value == 4;
	}

	/**
	 * Checks, whether the ack is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 5 && {@link #getValue()} <= 10
	 */
	public boolean isInCompatibleRange() {
		return value >= 5 && value <= 10;
	}

	/**
	 * Checks, whether the ack is in private range.
	 * 
	 * @return {@link #getValue()} >= 11 && {@link #getValue()} <= 15
	 */
	public boolean isInPrivateRange() {
		return value >= 11 && value <= 15;
	}

	/**
	 * Returns the file error.
	 * 
	 * @return File errors can be:
	 *         <ul>
	 *         <li>default</li>
	 *         <li>not enough memory</li>
	 *         <li>checksum failed</li>
	 *         <li>unexpected communication service</li>
	 *         <li>unexpected name of file</li>
	 *         <li>unexpected name of section</li>
	 *         <li>in compatible range</li>
	 *         <li>in private range</li>
	 *         </ul>
	 */
	public FileError getError() {
		return error;
	}

	/**
	 * Sets the file error.
	 * 
	 * @param error File errors can be:
	 *              <ul>
	 *              <li>default</li>
	 *              <li>not enough memory</li>
	 *              <li>checksum failed</li>
	 *              <li>unexpected communication service</li>
	 *              <li>unexpected name of file</li>
	 *              <li>unexpected name of section</li>
	 *              <li>in compatible range</li>
	 *              <li>in private range</li>
	 *              </ul>
	 */
	public void setError(FileError error) {
		this.error = error;
	}

	/**
	 * Empty default constructor.
	 */
	public AckFileOrSegmentQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = not used</li>
	 *              <li>1 = positive ack of file</li>
	 *              <li>2 = negative ack of file</li>
	 *              <li>3 = positive ack of section</li>
	 *              <li>4 = negative ack of section</li>
	 *              <li>5-10 = in compatible range</li>
	 *              <li>11-15 = in private range</li>
	 *              </ul>
	 * @param error File errors can be:
	 *              <ul>
	 *              <li>default</li>
	 *              <li>not enough memory</li>
	 *              <li>checksum failed</li>
	 *              <li>unexpected communication service</li>
	 *              <li>unexpected name of file</li>
	 *              <li>unexpected name of section</li>
	 *              <li>in compatible range</li>
	 *              <li>in private range</li>
	 *              </ul>
	 */
	public AckFileOrSegmentQualifier(int value, FileError error) {
		this.value = value;
		this.error = error;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((error == null) ? 0 : error.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AckFileOrSegmentQualifier other = (AckFileOrSegmentQualifier) obj;
		if (error == null) {
			if (other.error != null) {
				return false;
			}
		} else if (!error.equals(other.error)) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = byteAsInt & 0x0f;
		error = new FileError(byteAsInt >> 4);
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) (value | error.getValue() << 4) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}