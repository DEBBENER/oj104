/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a checksum.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class Checksum extends ByteValue implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -6961946732480924993L;

	/**
	 * Empty default constructor.
	 */
	public Checksum() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value The checksum.
	 */
	public Checksum(byte value) {
		super(value);
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}