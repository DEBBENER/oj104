/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * <<<<<<< Updated upstream Information element that contains a regulating step
 * command with three possible values.
 * <p>
 * Abbreviation: RCO
 * <p>
 * <ul>
 * <li>next step lower</li>
 * <li>next step higher</li>
 * <li>not permitted</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "state" })
public class RegulatingStepCommand implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 8020074070027904547L;

	/**
	 * Enumeration for regulating step command values.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	public enum RegulatingStepCommandState implements Serializable {

		/**
		 * Next step lower.
		 */
		NEXT_STEP_LOWER(0x01),

		/**
		 * Next step higher.
		 */
		NEXT_STEP_HIGHER(0x02),

		/**
		 * Not permitted.
		 */
		NOT_PERMITTED(0x00, 0x03);

		/**
		 * The codes of the command: ul>
		 * <li>[0x00, 0x03] = not permitted</li>
		 * <li>[0x01] = next step lower</li>
		 * <li>[0x02] = next step lower</li>
		 * </ul>
		 */
		private int[] codes;

		/**
		 * Re
		 * 
		 * @return An integer array in:
		 *         <ul>
		 *         <li>[0x00, 0x03] = not permitted</li>
		 *         <li>[0x01] = next step lower</li>
		 *         <li>[0x02] = next step lower</li>
		 *         </ul>
		 */
		public int[] getCodes() {
			return codes;
		}

		/**
		 * Constructor with fields.
		 * 
		 * @param codes An integer array in:
		 *              <ul>
		 *              <li>[0x00, 0x03] = not permitted</li>
		 *              <li>[0x01] = next step lower</li>
		 *              <li>[0x02] = next step lower</li>
		 *              </ul>
		 */
		private RegulatingStepCommandState(int... codes) {
			this.codes = codes;
		}

		/**
		 * Retrieve an enum entry.
		 * 
		 * @param code The code of the command:
		 *             <ul>
		 *             <li>0x00, 0x03 = not permitted</li>
		 *             <li>0x01 = next step lower</li>
		 *             <li>0x02 = next step lower</li>
		 *             </ul>
		 * @return An optional of the {@link RegulatingStepCommandState} or
		 *         {@link Optional#empty()}, if there is none for the code.
		 */
		@SuppressWarnings("unlikely-arg-type")
		public static Optional<RegulatingStepCommandState> getCommandState(int code) {
			return Arrays.asList(values()).stream().filter(state -> Arrays.asList(state.getCodes()).contains(code))
					.findAny();
		}
	}

	/**
	 * The code of the command state.
	 * <p>
	 * Abbreviation: RCS
	 * <p>
	 * Can have the following values:
	 * <ul>
	 * <li>{@link RegulatingStepCommandState#NOT_PERMITTED}</li>
	 * <li>{@link RegulatingStepCommandState#NEXT_STEP_LOWER}</li>
	 * <li>{@link RegulatingStepCommandState#NEXT_STEP_HIGHER}</li>
	 * </ul>
	 */
	private int stateCode;

	/**
	 * The command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * <p>
	 * Can have the following values:
	 * <ul>
	 * <li>0 = no additional information</li>
	 * <li>1 = short pulse duration</li>
	 * <li>2 = long pulse duration</li>
	 * <li>3 = persistent output</li>
	 * <li>4-8 = in compatible range</li>
	 * <li>9-15 = reserved for other predefined functions</li>
	 * <li>16-31 = in private range</li>
	 * </ul>
	 */
	private CommandQualifier qualifier;

	/**
	 * Returns the code of the command state.
	 * <p>
	 * Abbreviation: RCS
	 * 
	 * @return One of the following values:
	 *         <ul>
	 *         <li>{@link RegulatingStepCommandState#NOT_PERMITTED}</li>
	 *         <li>{@link RegulatingStepCommandState#NEXT_STEP_LOWER}</li>
	 *         <li>{@link RegulatingStepCommandState#NEXT_STEP_HIGHER}</li>
	 *         </ul>
	 */
	public int getStateCode() {
		return stateCode;
	}

	/**
	 * Returns the command state.
	 * 
	 * @return One of the following values:
	 *         <ul>
	 *         <li>{@link RegulatingStepCommandState#NOT_PERMITTED}</li>
	 *         <li>{@link RegulatingStepCommandState#NEXT_STEP_LOWER}</li>
	 *         <li>{@link RegulatingStepCommandState#NEXT_STEP_HIGHER}</li>
	 *         </ul>
	 */
	public Optional<RegulatingStepCommandState> getState() {
		return RegulatingStepCommandState.getCommandState(stateCode);
	}

	/**
	 * Sets the code of the command state.
	 * <p>
	 * Abbreviation: RCS
	 * 
	 * @param stateCode One of the following values:
	 *                  <ul>
	 *                  <li>{@link RegulatingStepCommandState#NOT_PERMITTED}</li>
	 *                  <li>{@link RegulatingStepCommandState#NEXT_STEP_LOWER}</li>
	 *                  <li>{@link RegulatingStepCommandState#NEXT_STEP_HIGHER}</li>
	 *                  </ul>
	 */
	public void setStateCode(int stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * Returns the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = no additional information</li>
	 *         <li>1 = short pulse duration</li>
	 *         <li>2 = long pulse duration</li>
	 *         <li>3 = persistent output</li>
	 *         <li>4-8 = in compatible range</li>
	 *         <li>9-15 = reserved for other predefined functions</li>
	 *         <li>16-31 = in private range</li>
	 *         </ul>
	 */
	public CommandQualifier getQualifier() {
		return qualifier;
	}

	/**
	 * Sets the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @param qualifier An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range</li>
	 *                  </ul>
	 */
	public void setQualifier(CommandQualifier qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * Empty default constructor.
	 */
	public RegulatingStepCommand() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param stateCode The code of the command state.
	 *                  <p>
	 *                  Abbreviation: RCS
	 *                  <p>
	 *                  Can have the following values:
	 *                  <ul>
	 *                  <li>{@link RegulatingStepCommandState#NOT_PERMITTED}</li>
	 *                  <li>{@link RegulatingStepCommandState#NEXT_STEP_LOWER}</li>
	 *                  <li>{@link RegulatingStepCommandState#NEXT_STEP_HIGHER}</li>
	 *                  </ul>
	 * @param qualifier The command qualifier.
	 *                  <p>
	 *                  Abbreviation: QOC
	 *                  <p>
	 *                  An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range
	 */
	public RegulatingStepCommand(int stateCode, CommandQualifier qualifier) {
		this.stateCode = stateCode;
		this.qualifier = qualifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((qualifier == null) ? 0 : qualifier.hashCode());
		result = prime * result + stateCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RegulatingStepCommand other = (RegulatingStepCommand) obj;
		if (qualifier == null) {
			if (other.qualifier != null) {
				return false;
			}
		} else if (!qualifier.equals(other.qualifier)) {
			return false;
		}
		if (stateCode != other.stateCode) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		stateCode = byteAsInt & 0x03;
		qualifier = new CommandQualifier();
		byte[] remainingBytes = qualifier.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualifier.toBytes();
		qdsBytes[0] |= stateCode;
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}