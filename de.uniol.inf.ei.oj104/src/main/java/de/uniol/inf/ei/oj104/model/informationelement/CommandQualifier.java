/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a qualifier for a command.
 * <p>
 * Abbreviation: QOC
 * <p>
 * <ul>
 * <li>no additional information</li>
 * <li>short pulse duration</li>
 * <li>long pulse duration</li>
 * <li>persistent output</li>
 * <li>in compatible range</li>
 * <li>reserved for other predefined functions</li>
 * <li>in private range</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonIgnoreProperties({ "inPrivateRange", "inCompatibleRange", "reservedForOtherPredefinedFunctions",
		"persistentOutput", "noAdditionalDef", "longPulseDuration", "shortPulseDuration" })
public class CommandQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 5315004349839264091L;

	/**
	 * The qualifier.
	 * <p>
	 * Abbreviation: QU
	 * <p>
	 * <ul>
	 * <li>0 = no additional information</li>
	 * <li>1 = short pulse duration</li>
	 * <li>2 = long pulse duration</li>
	 * <li>3 = persistent output</li>
	 * <li>4-8 = in compatible range</li>
	 * <li>9-15 = reserved for other predefined functions</li>
	 * <li>16-31 = in private range</li>
	 * </ul>
	 */
	private int value;

	/**
	 * Flag to indicate, whether it is a selection (true) or execution (false).
	 * <p>
	 * Abbreviation: S/E.
	 */
	private boolean select;

	/**
	 * Returns the qualifier.
	 * <p>
	 * Abbreviation: QU
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = no additional information</li>
	 *         <li>1 = short pulse duration</li>
	 *         <li>2 = long pulse duration</li>
	 *         <li>3 = persistent output</li>
	 *         <li>4-8 = in compatible range</li>
	 *         <li>9-15 = reserved for other predefined functions</li>
	 *         <li>16-31 = in private range</li>
	 *         </ul>
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the qualifier.
	 * <p>
	 * Abbreviation: QU
	 * 
	 * @param value An integer in:
	 *              <ul>
	 *              <li>0 = no additional information</li>
	 *              <li>1 = short pulse duration</li>
	 *              <li>2 = long pulse duration</li>
	 *              <li>3 = persistent output</li>
	 *              <li>4-8 = in compatible range</li>
	 *              <li>9-15 = reserved for other predefined functions</li>
	 *              <li>16-31 = in private range</li>
	 *              </ul>
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Checks, whether the qualifier means no additional definitions.
	 * 
	 * @return {@link #getValue()} == 0.
	 */
	public boolean isNoAdditionalDef() {
		return value == 0;
	}

	/**
	 * Checks, whether the qualifier means short pulse duration.
	 * 
	 * @return {@link #getValue()} == 1.
	 */
	public boolean isShortPulseDuration() {
		return value == 1;
	}

	/**
	 * Checks, whether the qualifier means long pulse duration.
	 * 
	 * @return {@link #getValue()} == 2.
	 */
	public boolean isLongPulseDuration() {
		return value == 2;
	}

	/**
	 * Checks, whether the qualifier means persistent output.
	 * 
	 * @return {@link #getValue()} == 3.
	 */
	public boolean isPersistentOutput() {
		return value == 3;
	}

	/**
	 * Checks, whether the qualifier is in compatible range.
	 * 
	 * @return {@link #getValue()} >= 4 && {@link #getValue()} <= 8.
	 */
	public boolean isInCompatibleRange() {
		return value >= 4 && value <= 8;
	}

	/**
	 * Checks, whether the qualifier is reserved for other predefined functions.
	 * 
	 * @return {@link #getValue()} >= 9 && {@link #getValue()} <= 15.
	 */
	public boolean isReservedForOtherPredefinedFunctions() {
		return value >= 9 && value <= 15;
	}

	/**
	 * Checks, whether the qualifier is in private range.
	 * 
	 * @return {@link #getValue()} >= 16 && {@link #getValue()} <= 31.
	 */
	public boolean isInPrivateRange() {
		return value >= 16 && value <= 31;
	}

	/**
	 * Checks, whether it is a selection.
	 * <p>
	 * Abbreviation: S/E.
	 * 
	 * @return True, if the qualifier is a selection; false, if it is an execution.
	 */
	public boolean isSelect() {
		return select;
	}

	/**
	 * Sets, whether it is a selection.
	 * <p>
	 * Abbreviation: S/E.
	 * 
	 * @param select True, if the qualifier is a selection; false, if it is an
	 *               execution.
	 */
	public void setSelect(boolean select) {
		this.select = select;
	}

	/**
	 * Checks, whether it is an execution.
	 * <p>
	 * Abbreviation: S/E.
	 * 
	 * @return True, if the qualifier is an execution; false, if it is a selection.
	 */
	public boolean isExecute() {
		return !select;
	}

	/**
	 * Sets, whether it is an execution.
	 * <p>
	 * Abbreviation: S/E.
	 * 
	 * @param execute True, if the qualifier is an execution; false, if it is a
	 *                selection.
	 */
	public void setExecute(boolean execute) {
		select = !execute;
	}

	/**
	 * Empty default constructor.
	 */
	public CommandQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value  The qualifier, an integer in:
	 *               <ul>
	 *               <li>0 = no additional information</li>
	 *               <li>1 = short pulse duration</li>
	 *               <li>2 = long pulse duration</li>
	 *               <li>3 = persistent output</li>
	 *               <li>4-8 = in compatible range</li>
	 *               <li>9-15 = reserved for other predefined functions</li>
	 *               <li>16-31 = in private range</li>
	 *               </ul>
	 * @param select Flag to indicate, whether it is a selection (true) or execution
	 *               (false).
	 *               <p>
	 *               Abbreviation: S/E.
	 */
	public CommandQualifier(int value, boolean select) {
		this.select = select;
		this.value = value;
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = (byteAsInt & 0x7c) >> 2;
		select = (byteAsInt & 0x80) == 0x80;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (select ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommandQualifier other = (CommandQualifier) obj;
		if (select != other.select) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) ((value << 2) | (select ? 0x80 : 0x00)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}