/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.io.Serializable;

import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * The cause of transmission directs the ASDU to a specific application task
 * (program) for processing.
 * <p>
 * It has an id and a description. The ids are defined in the standard. The ids
 * range from 0 to 63. The description is not necessary to be machine readable
 * but may be useful for humans.
 * 
 * @author @author Michael Brand (michael.brand@uol.de)
 *
 */
public class CauseOfTransmission implements Serializable {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 1832467573930575095L;

	/**
	 * The unique id of the cause of transmission in [0, 63].
	 */
	private int id;

	/**
	 * The description is not necessary to be machine readable but may be useful for
	 * humans.
	 */
	private String description;

	/**
	 * Returns the unique id of thecause of transmission.
	 * 
	 * @return An integer in [0, 63].
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the description of the cause of transmission.
	 * 
	 * @return A String describing the cause of transmission for humans.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Empty default constructor.
	 */
	public CauseOfTransmission() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param id          The unique id of the cause of transmission in [0, 63].
	 * @param description The description is not necessary to be machine readable
	 *                    but may be useful for humans.
	 */
	public CauseOfTransmission(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CauseOfTransmission other = (CauseOfTransmission) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

}