/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.RandomASDUCreator;

/**
 * Class that contains test for {@link ASDU}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ASDUTest {

	/**
	 * Factory to create random {@link ASDU}s for the tests.
	 */
	private final RandomASDUCreator factory = new RandomASDUCreator();

	/**
	 * Test the conversion of {@link ASDU}s with type ID 1 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId1() {
		final int asduTypeId = 1;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 2 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId2() {
		final int asduTypeId = 2;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 3 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId3() {
		final int asduTypeId = 3;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 4 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId4() {
		final int asduTypeId = 4;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 5 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId5() {
		final int asduTypeId = 5;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 6 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId6() {
		final int asduTypeId = 6;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 7 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId7() {
		final int asduTypeId = 7;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 8 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId8() {
		final int asduTypeId = 8;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 9 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId9() {
		final int asduTypeId = 9;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 10 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId10() {
		final int asduTypeId = 10;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 11 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId11() {
		final int asduTypeId = 11;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 12 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId12() {
		final int asduTypeId = 12;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 13 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId13() {
		final int asduTypeId = 13;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 14 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId14() {
		final int asduTypeId = 14;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 15 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId15() {
		final int asduTypeId = 15;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 16 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId16() {
		final int asduTypeId = 16;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 17 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId17() {
		final int asduTypeId = 17;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 18 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId18() {
		final int asduTypeId = 18;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 19 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId19() {
		final int asduTypeId = 19;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 20 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId20() {
		final int asduTypeId = 20;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 21 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId21() {
		final int asduTypeId = 21;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 30 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId30() {
		final int asduTypeId = 30;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 31 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId31() {
		final int asduTypeId = 31;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 32 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId32() {
		final int asduTypeId = 32;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 33 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId33() {
		final int asduTypeId = 33;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 34 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId34() {
		final int asduTypeId = 34;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 35 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId35() {
		final int asduTypeId = 35;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 36 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId36() {
		final int asduTypeId = 36;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 37 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId37() {
		final int asduTypeId = 37;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 38 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId38() {
		final int asduTypeId = 38;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 39 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId39() {
		final int asduTypeId = 39;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 40 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId40() {
		final int asduTypeId = 40;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 45 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId45() {
		final int asduTypeId = 45;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 46 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId46() {
		final int asduTypeId = 46;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 47 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId47() {
		final int asduTypeId = 47;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 48 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId48() {
		final int asduTypeId = 48;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 49 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId49() {
		final int asduTypeId = 49;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 50 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId50() {
		final int asduTypeId = 50;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 51 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId51() {
		final int asduTypeId = 51;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 58 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId58() {
		final int asduTypeId = 58;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 59 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId59() {
		final int asduTypeId = 59;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 60 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId60() {
		final int asduTypeId = 60;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 61 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId61() {
		final int asduTypeId = 61;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 62 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId62() {
		final int asduTypeId = 62;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 63 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId63() {
		final int asduTypeId = 63;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 64 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId64() {
		final int asduTypeId = 64;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 70 to bytes and, afterward,
	 * conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId70() {
		final int asduTypeId = 70;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 100 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId100() {
		final int asduTypeId = 100;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 101 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId101() {
		final int asduTypeId = 101;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 102 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId102() {
		final int asduTypeId = 102;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 103 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId103() {
		final int asduTypeId = 103;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 104 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId104() {
		final int asduTypeId = 104;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 105 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId105() {
		final int asduTypeId = 105;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 106 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId106() {
		final int asduTypeId = 106;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 107 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId107() {
		final int asduTypeId = 107;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 110 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId110() {
		final int asduTypeId = 110;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 111 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId111() {
		final int asduTypeId = 111;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 112 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId112() {
		final int asduTypeId = 112;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 113 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId113() {
		final int asduTypeId = 113;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 120 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId120() {
		final int asduTypeId = 120;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 121 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId121() {
		final int asduTypeId = 121;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 122 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId122() {
		final int asduTypeId = 122;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 123 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId123() {
		final int asduTypeId = 123;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 124 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId124() {
		final int asduTypeId = 124;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 125 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId125() {
		final int asduTypeId = 125;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

	/**
	 * Test the conversion of {@link ASDU}s with type ID 126 to bytes and,
	 * afterward, conversion back from bytes.
	 */
	@Test
	public void testToBytesFromBytesASDUTypeId126() {
		final int asduTypeId = 126;
		List<ASDU> asdus = factory.createASDUs(asduTypeId);
		assertTrue("List of ASDUs with id " + asduTypeId + " is empty!", !asdus.isEmpty());

		for (ASDU asdu : asdus) {
			byte[] asduBinary = null;
			try {
				asduBinary = asdu.toBytes();
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			ASDU asdu2 = new ASDU();
			try {
				asdu2.fromBytes(asduBinary);
			} catch (IEC608705104ProtocolException e) {
				fail("ASDU id = " + asduTypeId + ". " + e.getMessage());
			}

			assertEquals("ASDUs " + asdu + " and " + asdu2 + " are not equal!", asdu, asdu2);
		}
	}

}