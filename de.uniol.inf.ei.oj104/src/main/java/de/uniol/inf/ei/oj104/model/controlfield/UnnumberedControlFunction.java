/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.controlfield;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APCI;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.IControlField;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * The unnumbered control function is the control field for {@link APDU}s in
 * U-format. In U-format, {@link APDU}s don't have an {@link ASDU} as payload.
 * <p>
 * An unnumbered control function control field is part of an {@link APCI} and
 * defines the following control information: {@link ControlFunctionType} and
 * activation or confirmation.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class UnnumberedControlFunction implements IControlField {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 6028542126615969356L;

	/**
	 * A control function type is the content of an
	 * {@link UnnumberedControlFunction} with the flag activation or confirm.
	 * Possible types are {@link ControlFunctionType#STARTDT},
	 * {@link ControlFunctionType#STOPDT}, and {@link ControlFunctionType#TESTFR}.
	 */
	private ControlFunctionType type;

	/**
	 * True, if the {@link APDU} marks the activation of a control function. False,
	 * if the {@link APDU} marks the confirmation of a control function.
	 */
	private boolean activate;

	/**
	 * Returns the control function type.
	 * 
	 * @return Possible types are {@link ControlFunctionType#STARTDT},
	 *         {@link ControlFunctionType#STOPDT}, and
	 *         {@link ControlFunctionType#TESTFR}.
	 */
	public ControlFunctionType getType() {
		return type;
	}

	/**
	 * Sets the control function type.
	 * 
	 * @param type Possible types are {@link ControlFunctionType#STARTDT},
	 *             {@link ControlFunctionType#STOPDT}, and
	 *             {@link ControlFunctionType#TESTFR}.
	 */
	public void setType(ControlFunctionType type) {
		this.type = type;
	}

	/**
	 * Returns whether the {@link APDU} marks the activation of a control function.
	 * 
	 * @return True, if the {@link APDU} marks the activation of a control function.
	 *         False, if the {@link APDU} marks the confirmation of a control
	 *         function.
	 */
	public boolean isActivate() {
		return activate;
	}

	/**
	 * Sets whether the {@link APDU} marks the activation of a control function.
	 * 
	 * @param activate True, if the {@link APDU} marks the activation of a control
	 *                 function. False, if the {@link APDU} marks the confirmation
	 *                 of a control function.
	 */
	public void setActivate(boolean activate) {
		this.activate = activate;
	}

	/**
	 * Returns whether the {@link APDU} marks the confirmation of a control
	 * function.
	 * 
	 * @return True, if the {@link APDU} marks the confirmation of a control
	 *         function. False, if the {@link APDU} marks the activation of a
	 *         control function.
	 */
	public boolean isConfirm() {
		return !activate;
	}

	/**
	 * Sets whether the {@link APDU} marks the confirmation of a control function.
	 * 
	 * @param confirm True, if the {@link APDU} marks the confirmation of a control
	 *                function. False, if the {@link APDU} marks the activation of a
	 *                control function.
	 */
	public void setConform(boolean confirm) {
		activate = !confirm;
	}

	/**
	 * Empty default constructor.
	 */
	public UnnumberedControlFunction() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param type     A control function type is the content of an
	 *                 {@link UnnumberedControlFunction} with the flag activation or
	 *                 confirm. Possible types are
	 *                 {@link ControlFunctionType#STARTDT},
	 *                 {@link ControlFunctionType#STOPDT}, and
	 *                 {@link ControlFunctionType#TESTFR}.
	 * @param activate True, if the {@link APDU} marks the activation of a control
	 *                 function. False, if the {@link APDU} marks the confirmation
	 *                 of a control function.
	 */
	public UnnumberedControlFunction(ControlFunctionType type, boolean activate) {
		this.type = type;
		this.activate = activate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activate ? 1231 : 1237);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UnnumberedControlFunction other = (UnnumberedControlFunction) obj;
		if (activate != other.activate) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < IControlField.getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), IControlField.getEncodedSize(), bytes.length);
		}

		type = ControlFunctionType.getType(bytes[0]).orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
				"Can not determine unnumbered control function from " + bytes[0] + "!"));
		activate = type.getActivate() == bytes[0];

		for (int i = 1; i < IControlField.getEncodedSize(); i++) {
			if (bytes[i] != 0) {
				type = null;
				throw new IEC608705104ProtocolException(getClass(),
						(i + 1) + ". octet of numbered control function must be 0 but is " + bytes[i] + "!");
			}
		}
		return Arrays.copyOfRange(bytes, IControlField.getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[IControlField.getEncodedSize()];
		bytes[0] = (byte) (activate ? type.getActivate() : type.getConfirm());
		for (int i = 1; i < 4; i++) {
			bytes[i] = 0x00;
		}
		return bytes;
	}

}
