/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.timetag;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.TimeZone;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Time tag with information about year, month, day, minutes, seconds, and
 * milliseconds.
 * <p>
 * Time tags are special {@link IInformationElement}s.
 * <p>
 * Abbreviation: CP56Time2a
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SevenOctetBinaryTime extends ThreeOctetBinaryTime {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 2994065017692369969L;

	/**
	 * The hours of the time tag (>= 0 and < 24).
	 */
	private int hours;

	/**
	 * The month day of the time tag (> 0 and <= 31).
	 */
	private int daysOfMonth;

	/**
	 * The week day of the time tag (> 0 and <= 7) with 1 = Monday.
	 */
	private int daysOfWeek;

	/**
	 * The months of the time tag (> 0 and <= 12) with 1 = January.
	 */
	private int months;

	/**
	 * The years of the time tag with only two digits (>= 0 and < 100).
	 */
	private int years;

	/**
	 * The calendar instance representing the time tag.
	 */
	private Calendar calendar;

	@Override
	public void setMilliseconds(int milliseconds) {
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.MILLISECOND, milliseconds));
		super.setMilliseconds(milliseconds);
	}

	@Override
	public void setSeconds(int seconds) {
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.SECOND, seconds));
		super.setSeconds(seconds);
	}

	@Override
	public void setMinutes(int minutes) {
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.MINUTE, minutes));
		super.setMinutes(minutes);
	}

	/**
	 * Returns the hours of the time tag.
	 * 
	 * @return An integer in [0;24)
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * Sets the hours of the time tag.
	 * 
	 * @param hours An integer in [0;24)
	 */
	public void setHours(int hours) {
		// Calendar.HOUR is only 0-11
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.HOUR_OF_DAY, hours));
		this.hours = hours;
	}

	/**
	 * Returns the month day of the time tag.
	 * 
	 * @return An integer in (0;31].
	 */
	public int getDaysOfMonth() {
		return daysOfMonth;
	}

	/**
	 * Sets the month day of the time tag.
	 * 
	 * @param daysOfMonth An integer in (0;31].
	 */
	public void setDaysOfMonth(int daysOfMonth) {
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.DAY_OF_MONTH, daysOfMonth));
		this.daysOfMonth = daysOfMonth;
	}

	/**
	 * Returns the week day of the time tag.
	 * 
	 * @return An integer in (0;7] with 1 = Monday.
	 */
	public int getDaysOfWeek() {
		return daysOfWeek;
	}

	/**
	 * Sets the week day of the time tag.
	 * 
	 * @param daysOfWeek An integer in (0;7] with 1 = Monday.
	 */
	public void setDaysOfWeek(int daysOfWeek) {
		// Calendar.DAY_OF_WEEK: 1 = Sunday, 7 = Saturday
		// SevenOctetBinaryTime.daysOfWeek: 1 = Monday, 7 = Sunday
		// adding 1 and modulo operation results in 0 for Saturday, but should be 7
		final int calDaysOfWeek = (daysOfWeek + 1) % 7 == 0 ? 7 : (daysOfWeek + 1) % 7;
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.DAY_OF_WEEK, calDaysOfWeek));
		this.daysOfWeek = daysOfWeek;
	}

	/**
	 * Returns the months of the time tag.
	 * 
	 * @return An integer in (0;12] with 1 = January.
	 */
	public int getMonths() {
		return months;
	}

	/**
	 * Sets the months of the time tag.
	 * 
	 * @param months An integer in (0;12] with 1 = January.
	 */
	public void setMonths(int months) {
		// Calendar.MONTH: 0 = January, 11 = December
		// SevenOctetBinaryTime.months: 1 = January, 12 = December
		Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.MONTH, months - 1));
		this.months = months;
	}

	/**
	 * Returns the years of the time tag with only two digits.
	 * 
	 * @return An integer in [0;100).
	 */
	public int getYears() {
		return years;
	}

	/**
	 * Sets the years of the time tag.
	 * 
	 * @param years   An integer in [0;100).
	 * @param century An integer in [0;100) or {@link Optional#empty()}.
	 */
	public void setYears(int years, Optional<Integer> century) {
		// year has only two digits in SevenOctetBinaryTime
		century.ifPresent(c -> Optional.ofNullable(calendar).ifPresent(cal -> cal.set(Calendar.YEAR, c * 100 + years)));
		this.years = years;
	}

	/**
	 * Returns the time tag as calendar object.
	 * 
	 * @param timezone The time zone of the time tag.
	 * @param century  An integer in [0;100).
	 * @return A calendar object representing the time tag.
	 */
	public Calendar asCalendar(TimeZone timezone, int century) {
		if (calendar == null) {
			calendar = Calendar.getInstance(timezone);
			// year has only two digits in SevenOctetBinaryTime
			calendar.set(Calendar.YEAR, century * 100 + years);
			// Calendar.MONTH: 0 = January, 11 = December
			// SevenOctetBinaryTime.months: 1 = January, 12 = December
			calendar.set(Calendar.MONTH, months - 1);
			if (daysOfWeek > 0) {
				// Calendar.DAY_OF_WEEK: 1 = Sunday, 7 = Saturday
				// SevenOctetBinaryTime.daysOfWeek: 1 = Monday, 7 = Sunday
				// adding 1 and modulo operation results in 0 for Saturday, but should be 7
				int dow = (daysOfWeek + 1) % 7;
				if (dow == 0) {
					dow = 7;
				}
				calendar.set(Calendar.DAY_OF_WEEK, dow);
			}
			calendar.set(Calendar.DAY_OF_MONTH, daysOfMonth);
			// Calendar.HOUR is only 0-11
			calendar.set(Calendar.HOUR_OF_DAY, hours);
			calendar.set(Calendar.MINUTE, getMinutes());
			calendar.set(Calendar.SECOND, getSeconds());
			calendar.set(Calendar.MILLISECOND, getMilliseconds());
			calendar.setLenient(false);
		}
		return calendar;
	}

	/**
	 * Reads the time tag from a calendar object.
	 * 
	 * @param calendar A calendar object.
	 */
	public void fromCalendar(Calendar calendar) {
		this.calendar = calendar;
		super.setMilliseconds(calendar.get(Calendar.MILLISECOND));
		super.setSeconds(calendar.get(Calendar.SECOND));
		super.setMinutes(calendar.get(Calendar.MINUTE));
		// Calendar.HOUR is only 0-11
		hours = calendar.get(Calendar.HOUR_OF_DAY);
		daysOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		// Calendar.DAY_OF_WEEK: 1 = Sunday, 7 = Saturday
		// SevenOctetBinaryTime.daysOfWeek: 1 = Monday, 7 = Sunday
		// subtracting 1 results in 0 for Sunday, but should be 7
		int dow = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (dow == 0) {
			dow = 7;
		}
		daysOfWeek = dow;
		// Calendar.MONTH: 0 = January, 11 = December
		// SevenOctetBinaryTime.months: 1 = January, 12 = December
		months = calendar.get(Calendar.MONTH) + 1;
		// year has only two digits in SevenOctetBinaryTime
		years = calendar.get(Calendar.YEAR) % 100;
	}

	@Override
	public long getTimestamp() {
		return getTimestamp(TimeZone.getDefault(),
				Calendar.getInstance(TimeZone.getDefault()).get(Calendar.YEAR) / 100);
	}

	/**
	 * Returns the time tag as time stamp.
	 * 
	 * @param timezone The time zone of the time tag.
	 * @param century  An integer in [0;100).
	 * @return A unix time stamp.
	 */
	public long getTimestamp(TimeZone timezone, int century) {
		return asCalendar(timezone, century).getTimeInMillis();
	}

	/**
	 * Checks, whether the point in time of the time tag is is summer time.
	 * <p>
	 * Abbreviation: SU
	 * 
	 * @param timezone The time zone of the time tag.
	 * @param century  An integer in [0;100).
	 * @return True, if the point in time of the time tag is is summer time.
	 */
	public boolean isSummerTime(TimeZone timezone, int century) {
		return timezone.inDaylightTime(asCalendar(timezone, century).getTime());
	}

	/**
	 * Empty default constructor.
	 */
	public SevenOctetBinaryTime() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param milliseconds The milliseconds of the time tag (>= 0 and < 1000).
	 * @param seconds      The seconds of the time tag (>= 0 and < 60).
	 * @param minutes      The minutes of the time tag (>= 0 and < 60).
	 * @param substituted  True, if the time tag is a substitution.
	 * @param invalid      True, if the time tag is invalid.
	 * @param hours        The hours of the time tag (>= 0 and < 24).
	 * @param daysOfMonth  The month day of the time tag (> 0 and <= 31).
	 * @param daysOfWeek   The week day of the time tag (> 0 and <= 7) with 1 =
	 *                     Monday.
	 * @param months       The months of the time tag (> 0 and <= 12) with 1 =
	 *                     January.
	 * @param years        The years of the time tag with only two digits (>= 0 and
	 *                     < 100).
	 */
	public SevenOctetBinaryTime(int milliseconds, int seconds, int minutes, boolean substituted, boolean invalid,
			int hours, int daysOfMonth, int daysOfWeek, int months, int years) {
		super(milliseconds, seconds, minutes, substituted, invalid);
		this.hours = hours;
		this.daysOfMonth = daysOfMonth;
		this.daysOfWeek = daysOfWeek;
		this.months = months;
		this.years = years;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + daysOfMonth;
		result = prime * result + daysOfWeek;
		result = prime * result + hours;
		result = prime * result + months;
		result = prime * result + years;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SevenOctetBinaryTime other = (SevenOctetBinaryTime) obj;
		if (daysOfMonth != other.daysOfMonth) {
			return false;
		}
		if (daysOfWeek != other.daysOfWeek) {
			return false;
		}
		if (hours != other.hours) {
			return false;
		}
		if (months != other.months) {
			return false;
		}
		if (years != other.years) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		byte[] remainingBytes = super.fromBytes(bytes);
		hours = remainingBytes[0] & 0x1f;
		daysOfMonth = remainingBytes[1] & 0x1f;
		daysOfWeek = (remainingBytes[1] & 0xe0) >> 5;
		months = remainingBytes[2] & 0x0f;
		years = remainingBytes[3] & 0x7f;
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] threeOctetBinaryTimeBytes = super.toBytes();
		byte hourByte = (byte) hours;
		if (calendar != null) {
			hourByte |= isSummerTime(calendar.getTimeZone(), calendar.get(Calendar.YEAR) / 100) ? 0x80 : 0x00;
		}
		byte[] bytes = ArrayUtils.add(threeOctetBinaryTimeBytes, hourByte);
		byte dayBytes = (byte) daysOfMonth;
		dayBytes |= (byte) (daysOfWeek << 5);
		bytes = ArrayUtils.add(bytes, dayBytes);
		byte monthByte = (byte) months;
		bytes = ArrayUtils.add(bytes, monthByte);
		byte yearByte = (byte) years;
		return ArrayUtils.add(bytes, yearByte);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 7;
	}

}