/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.util.Arrays;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IByteEncodedEntity;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * A short value to be used in {@link IInformationElement}s.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class ShortValue implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -5689591236414814300L;

	/**
	 * The short value.
	 */
	private short value;

	/**
	 * Returns the short value.
	 * 
	 * @return A short.
	 */
	public short getValue() {
		return value;
	}

	/**
	 * Sets the short value.
	 * 
	 * @param value A short.
	 */
	public void setValue(short value) {
		this.value = value;
	}

	/**
	 * Empty default constructor.
	 */
	public ShortValue() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value A short.
	 */
	public ShortValue(short value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ShortValue other = (ShortValue) obj;
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		value = (short) (bytes[0] & 0xff);
		for (int i = 1; i < getEncodedSize(); i++) {
			value |= (bytes[1] & 0xff) << (8 * i);
		}
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[getEncodedSize()];
		bytes[0] = (byte) value;
		for (int i = 1; i < getEncodedSize(); i++) {
			bytes[i] = (byte) (value >> (8 * i));
		}
		return bytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return Short.BYTES;
	}

}