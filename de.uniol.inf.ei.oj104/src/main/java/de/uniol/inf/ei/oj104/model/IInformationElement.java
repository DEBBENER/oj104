/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import de.uniol.inf.ei.oj104.model.informationelement.AckFileOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryCounterReading;
import de.uniol.inf.ei.oj104.model.informationelement.BinaryStateInformation;
import de.uniol.inf.ei.oj104.model.informationelement.CauseOfInitialization;
import de.uniol.inf.ei.oj104.model.informationelement.Checksum;
import de.uniol.inf.ei.oj104.model.informationelement.CommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.CounterInterrogationCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.DoubleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.DoublePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.FileOrSectionLength;
import de.uniol.inf.ei.oj104.model.informationelement.FileReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.FileStatus;
import de.uniol.inf.ei.oj104.model.informationelement.FixedTestBitPattern;
import de.uniol.inf.ei.oj104.model.informationelement.InterrogationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.LastSectionOrSegmentQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.MeasuredValuesParameterQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfFile;
import de.uniol.inf.ei.oj104.model.informationelement.NameOfSection;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.PEOutputCircuitInformation;
import de.uniol.inf.ei.oj104.model.informationelement.PESingleEvent;
import de.uniol.inf.ei.oj104.model.informationelement.PEStartEvent;
import de.uniol.inf.ei.oj104.model.informationelement.ParameterActivationQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorForPE;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.informationelement.RegulatingStepCommand;
import de.uniol.inf.ei.oj104.model.informationelement.ResetProcessCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ScaledValue;
import de.uniol.inf.ei.oj104.model.informationelement.SectionReadyQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.Segment;
import de.uniol.inf.ei.oj104.model.informationelement.SelectAndCallQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.SetPointCommandQualifier;
import de.uniol.inf.ei.oj104.model.informationelement.ShortFloatingPointNumber;
import de.uniol.inf.ei.oj104.model.informationelement.SingleCommand;
import de.uniol.inf.ei.oj104.model.informationelement.SinglePointInformation;
import de.uniol.inf.ei.oj104.model.informationelement.StatusAndChangeDetection;
import de.uniol.inf.ei.oj104.model.informationelement.ValueWithTransientStateIndication;

/**
 * Empty interface for information elements.
 * <p>
 * An information element contains the concrete information, e.g. a measurement,
 * and is part of an {@link IInformationObject}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
@JsonTypeInfo(use = NAME, include = PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = AckFileOrSegmentQualifier.class, name = "AckFileOrSegmentQualifier"),
		@JsonSubTypes.Type(value = BinaryCounterReading.class, name = "BinaryCounterReading"),
		@JsonSubTypes.Type(value = BinaryStateInformation.class, name = "BinaryStateInformation"),
		@JsonSubTypes.Type(value = CauseOfInitialization.class, name = "CauseOfInitialization"),
		@JsonSubTypes.Type(value = Checksum.class, name = "Checksum"),
		@JsonSubTypes.Type(value = CommandQualifier.class, name = "CommandQualifier"),
		@JsonSubTypes.Type(value = CounterInterrogationCommandQualifier.class, name = "CounterInterrogationCommandQualifier"),
		@JsonSubTypes.Type(value = DoubleCommand.class, name = "DoubleCommand"),
		@JsonSubTypes.Type(value = DoublePointInformation.class, name = "DoublePointInformation"),
		@JsonSubTypes.Type(value = FileOrSectionLength.class, name = "FileOrSectionLength"),
		@JsonSubTypes.Type(value = FileReadyQualifier.class, name = "FileReadyQualifier"),
		@JsonSubTypes.Type(value = FileStatus.class, name = "FileStatus"),
		@JsonSubTypes.Type(value = FixedTestBitPattern.class, name = "FixedTestBitPattern"),
		@JsonSubTypes.Type(value = InterrogationQualifier.class, name = "InterrogationQualifier"),
		@JsonSubTypes.Type(value = LastSectionOrSegmentQualifier.class, name = "LastSectionOrSegmentQualifier"),
		@JsonSubTypes.Type(value = MeasuredValuesParameterQualifier.class, name = "MeasuredValuesParameterQualifier"),
		@JsonSubTypes.Type(value = NameOfFile.class, name = "NameOfFile"),
		@JsonSubTypes.Type(value = NameOfSection.class, name = "NameOfSection"),
		@JsonSubTypes.Type(value = NormalizedValue.class, name = "NormalizedValue"),
		@JsonSubTypes.Type(value = ParameterActivationQualifier.class, name = "ParameterActivationQualifier"),
		@JsonSubTypes.Type(value = PEOutputCircuitInformation.class, name = "PEOutputCircuitInformation"),
		@JsonSubTypes.Type(value = PESingleEvent.class, name = "PESingleEvent"),
		@JsonSubTypes.Type(value = PEStartEvent.class, name = "PEStartEvent"),
		@JsonSubTypes.Type(value = QualityDescriptorForPE.class, name = "QualityDescriptorForPE"),
		@JsonSubTypes.Type(value = QualityDescriptorWithOV.class, name = "QualityDescriptorWithOV"),
		@JsonSubTypes.Type(value = RegulatingStepCommand.class, name = "RegulatingStepCommand"),
		@JsonSubTypes.Type(value = ResetProcessCommandQualifier.class, name = "ResetProcessCommandQualifier"),
		@JsonSubTypes.Type(value = ScaledValue.class, name = "ScaledValue"),
		@JsonSubTypes.Type(value = SectionReadyQualifier.class, name = "SectionReadyQualifier"),
		@JsonSubTypes.Type(value = Segment.class, name = "Segment"),
		@JsonSubTypes.Type(value = SelectAndCallQualifier.class, name = "SelectAndCallQualifier"),
		@JsonSubTypes.Type(value = SetPointCommandQualifier.class, name = "SetPointCommandQualifier"),
		@JsonSubTypes.Type(value = ShortFloatingPointNumber.class, name = "ShortFloatingPointNumber"),
		@JsonSubTypes.Type(value = SingleCommand.class, name = "SingleCommand"),
		@JsonSubTypes.Type(value = SinglePointInformation.class, name = "SinglePointInformation"),
		@JsonSubTypes.Type(value = StatusAndChangeDetection.class, name = "StatusAndChangeDetection"),
		@JsonSubTypes.Type(value = ValueWithTransientStateIndication.class, name = "ValueWithTransientStateIndication") })
public interface IInformationElement extends IByteEncodedEntity {
}