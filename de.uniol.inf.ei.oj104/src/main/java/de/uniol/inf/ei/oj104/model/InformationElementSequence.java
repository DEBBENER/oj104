/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Class for information element sequences. An information element sequence has
 * an ordered list of information elements and can have a time tag. The classes
 * of the information elements and the timetag are managed in the base class,
 * {@link AbstractInformationElementSequence}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class InformationElementSequence extends AbstractInformationElementSequence {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -752958450769599926L;

	/**
	 * An ordered list of the information elements, contained in the sequence.
	 */
	private List<IInformationElement> informationElements = new ArrayList<>();

	/**
	 * The time tag object of the sequence or <code>null</code>, if the sequence has
	 * no time tag.
	 */
	private ITimeTag timeTag;

	/**
	 * Returns the information elements, contained in the sequence.
	 * 
	 * @return An ordered list of the information elements, contained in the
	 *         sequence.
	 */
	public List<IInformationElement> getInformationElements() {
		return informationElements;
	}

	/**
	 * Sets the information elements, contained in the sequence.
	 * 
	 * @param informationElements An ordered list of the information elements,
	 *                            contained in the sequence. The elements must be of
	 *                            the correct classes, defined in
	 *                            {@link AbstractInformationElementSequence#getInformationElementClasses()}.
	 */
	public void setInformationElements(List<IInformationElement> informationElements) {
		this.informationElements = informationElements;
	}

	/**
	 * Returns the time tag object of the sequence.
	 * 
	 * @return The time tag object of the sequence or {@link Optional#empty()}, if
	 *         the sequence has no time tag.
	 */
	public Optional<ITimeTag> getTimeTag() {
		return Optional.ofNullable(timeTag);
	}

	/**
	 * Sets the time tag object of the sequence.
	 * 
	 * @param timeTag The time tag object for the sequence. Can be
	 *                <code>null</code>, if the sequence has no time tag. The time
	 *                tag must be of the correct class, defined in
	 *                {@link AbstractInformationElementSequence#getTimeTagClass()}.
	 */
	public void setTimeTag(Optional<ITimeTag> timeTag) {
		this.timeTag = timeTag.orElse(null);
	}

	/**
	 * Empty default constructor.
	 */
	public InformationElementSequence() {
	}

	/**
	 * Creates an empty sequence.
	 * 
	 * @param informationElementClasses An ordered list of the classes of the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 * @param timeTagClass              The class of the time tag object of the
	 *                                  sequence or <code>null</code>, if the
	 *                                  sequence has no time tag.
	 */
	public InformationElementSequence(List<Class<? extends IInformationElement>> informationElementClasses,
			Class<? extends ITimeTag> timeTagClass) {
		super(informationElementClasses, timeTagClass);
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param informationElementClasses An ordered list of the classes of the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 * @param informationElements       An ordered list of the information elements,
	 *                                  contained in the sequence. The elements must
	 *                                  be of the correct classes, defined in
	 *                                  <code>informationElementClasses</code>.
	 * @param timeTagClass              The class of the time tag object of the
	 *                                  sequence or <code>null</code>, if the
	 *                                  sequence has no time tag.
	 * @param timeTag                   The time tag object for the sequence. Can be
	 *                                  <code>null</code>, if the sequence has no
	 *                                  time tag. The time tag must be of the
	 *                                  correct class, defined in
	 *                                  <code>timeTagClass</code>.
	 */
	public InformationElementSequence(List<Class<? extends IInformationElement>> informationElementClasses,
			List<IInformationElement> informationElements, Class<? extends ITimeTag> timeTagClass, ITimeTag timeTag) {
		this(informationElementClasses, timeTagClass);
		this.informationElements = informationElements;
		this.timeTag = timeTag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((informationElements == null) ? 0 : informationElements.hashCode());
		result = prime * result + ((timeTag == null) ? 0 : timeTag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		InformationElementSequence other = (InformationElementSequence) obj;
		if (informationElements == null) {
			if (other.informationElements != null) {
				return false;
			}
		} else if (!informationElements.equals(other.informationElements)) {
			return false;
		}
		if (timeTag == null) {
			if (other.timeTag != null) {
				return false;
			}
		} else if (!timeTag.equals(other.timeTag)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		informationElements = new ArrayList<>(getInformationElementClasses().size());
		byte[] remainingBytes = bytes;
		for (Class<? extends IInformationElement> ieClass : getInformationElementClasses()) {
			try {
				IInformationElement informationElement = ieClass.newInstance();
				remainingBytes = informationElement.fromBytes(remainingBytes);
				informationElements.add(informationElement);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new IEC608705104ProtocolException(ieClass, "Can not instantiate!", e);
			}
		}

		ITimeTag tt = null;
		if (getTimeTagClass().isPresent()) {
			try {
				tt = getTimeTagClass().get().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new IEC608705104ProtocolException(getTimeTagClass().get(), "Can not instantiate!", e);
			}

			remainingBytes = tt.fromBytes(remainingBytes);
		}
		timeTag = tt;
		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] bytes = new byte[0];
		for (IInformationElement ie : informationElements) {
			byte[] ieBytes = ie.toBytes();
			bytes = ArrayUtils.addAll(bytes, ieBytes);
		}

		if (getTimeTagClass().isPresent()) {
			byte[] ttBytes = timeTag.toBytes();
			bytes = ArrayUtils.addAll(bytes, ttBytes);
		}
		return bytes;
	}

}