/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract base class for information element sequences. An information element
 * sequence has an ordered list of information elements and can have a time tag.
 * In this class, the classes of the information elements and the timetag are
 * managed.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public abstract class AbstractInformationElementSequence implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 7123777702261389552L;

	/**
	 * An ordered list of the classes of the information elements, contained in the
	 * sequence.
	 */
	private List<Class<? extends IInformationElement>> informationElementClasses = new ArrayList<>();

	/**
	 * The class of the time tag object of the sequence or <code>null</code>, if the
	 * sequence has no time tag.
	 */
	private Class<? extends ITimeTag> timeTagClass;

	/**
	 * Returns the classes of the information elements, contained in the sequence.
	 * 
	 * @return An ordered list of the classes of the information elements, contained
	 *         in the sequence.
	 */
	public List<Class<? extends IInformationElement>> getInformationElementClasses() {
		return informationElementClasses;
	}

	/**
	 * Sets the classes of the information elements, contained in the sequence.
	 * 
	 * @param informationElementClasses An ordered list of the classes for the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 */
	public void setInformationElementClasses(List<Class<? extends IInformationElement>> informationElementClasses) {
		this.informationElementClasses = informationElementClasses;
	}

	/**
	 * Returns the class of the time tag object of the sequence.
	 * 
	 * @return The class of the time tag object of the sequence or
	 *         {@link Optional#empty()}, if the sequence has no time tag.
	 */
	public Optional<Class<? extends ITimeTag>> getTimeTagClass() {
		return Optional.ofNullable(timeTagClass);
	}

	/**
	 * Sets the class of the time tag object of the sequence.
	 * 
	 * @param timeTagClass The class of the time tag object for the sequence. Can be
	 *                     <code>null</code>, if the sequence has no time tag.
	 */
	public void setTimeTagClass(Class<? extends ITimeTag> timeTagClass) {
		this.timeTagClass = timeTagClass;
	}

	/**
	 * Empty default constructor.
	 */
	public AbstractInformationElementSequence() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param informationElementClasses An ordered list of the classes of the
	 *                                  information elements, contained in the
	 *                                  sequence.
	 * @param timeTagClass              The class of the time tag object of the
	 *                                  sequence or <code>null</code>, if the
	 *                                  sequence has no time tag.
	 */
	public AbstractInformationElementSequence(List<Class<? extends IInformationElement>> informationElementClasses,
			Class<? extends ITimeTag> timeTagClass) {
		this.informationElementClasses = informationElementClasses;
		this.timeTagClass = timeTagClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((informationElementClasses == null) ? 0 : informationElementClasses.hashCode());
		result = prime * result + ((timeTagClass == null) ? 0 : timeTagClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractInformationElementSequence other = (AbstractInformationElementSequence) obj;
		if (informationElementClasses == null) {
			if (other.informationElementClasses != null) {
				return false;
			}
		} else if (!informationElementClasses.equals(other.informationElementClasses)) {
			return false;
		}
		if (timeTagClass == null) {
			if (other.timeTagClass != null) {
				return false;
			}
		} else if (!timeTagClass.equals(other.timeTagClass)) {
			return false;
		}
		return true;
	}

}