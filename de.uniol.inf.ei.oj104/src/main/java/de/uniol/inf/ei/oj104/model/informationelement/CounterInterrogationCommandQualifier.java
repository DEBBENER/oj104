/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a qualifier for a counter interrogation.
 * <p>
 * Abbreviation: QCC
 * <p>
 * It contains freeze information and the request for the counter interrogation.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class CounterInterrogationCommandQualifier implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -1943028923804416838L;

	/**
	 * Enumeration for freeze values.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	public enum Freeze implements Serializable {

		/**
		 * Read the counter (no freeze).
		 */
		READ(0),

		/**
		 * Freeze but do not reset the counter.
		 */
		COUNTER_FREEZE_WITHOUT_RESET(1),

		/**
		 * Freeze and reset the counter.
		 */
		COUNTER_FREEZE_WITH_RESET(2),

		/**
		 * Reset but to not freeze the counter.
		 */
		COUNTER_RESET(3);

		/**
		 * The code of the freeze:
		 * <ul>
		 * <li>0 = read</li>
		 * <li>1 = freeze but do not reset</li>
		 * <li>2 = freeze and reset</li>
		 * <li>3 = reset but do not freeze</li>
		 * </ul>
		 */
		private int code;

		/**
		 * Returns the code of the freeze.
		 * 
		 * @return An integer in:
		 *         <ul>
		 *         <li>0 = read</li>
		 *         <li>1 = freeze but do not reset</li>
		 *         <li>2 = freeze and reset</li>
		 *         <li>3 = reset but do not freeze</li>
		 *         </ul>
		 */
		public int getCode() {
			return code;
		}

		/**
		 * Constructor with fields.
		 * 
		 * @param code An integer in:
		 *             <ul>
		 *             <li>0 = read</li>
		 *             <li>1 = freeze but do not reset</li>
		 *             <li>2 = freeze and reset</li>
		 *             <li>3 = reset but do not freeze</li>
		 *             </ul>
		 */
		private Freeze(int code) {
			this.code = code;
		}

		/**
		 * Retrieve an enum entry.
		 * 
		 * @param code The code of the freeze:
		 *             <ul>
		 *             <li>0 = read</li>
		 *             <li>1 = freeze but do not reset</li>
		 *             <li>2 = freeze and reset</li>
		 *             <li>3 = reset but do not freeze</li>
		 *             </ul>
		 * @return An optional of the {@link Freeze} or {@link Optional#empty()}, if
		 *         there is none for the code.
		 */
		public static Optional<Freeze> getFreeze(int code) {
			return Arrays.asList(values()).stream().filter(freeze -> code == freeze.code).findAny();
		}
	}

	/**
	 * The counter interrogation command request:
	 * <ul>
	 * <li>0 = not used</li>
	 * <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no group</li>
	 * <li>5 = general request counter</li>
	 * <li>6-31 = in compatible range</li>
	 * <li>32 - 63 = in private range</li>
	 * </ul>
	 */
	private CounterInterrogationCommandRequest request;

	/**
	 * The freeze value:
	 * <ul>
	 * <li>0 = read</li>
	 * <li>1 = freeze but do not reset</li>
	 * <li>2 = freeze and reset</li>
	 * <li>3 = reset but do not freeze</li>
	 * </ul>
	 */
	private Freeze freeze;

	/**
	 * Returns the counter interrogation command request.
	 * 
	 * @return
	 *         <ul>
	 *         <li>0 = not used</li>
	 *         <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no group</li>
	 *         <li>5 = general request counter</li>
	 *         <li>6-31 = in compatible range</li>
	 *         <li>32 - 63 = in private range</li>
	 *         </ul>
	 */
	public CounterInterrogationCommandRequest getRequest() {
		return request;
	}

	/**
	 * Sets the counter interrogation command request.
	 * 
	 * @param request
	 *                <ul>
	 *                <li>0 = not used</li>
	 *                <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no
	 *                group</li>
	 *                <li>5 = general request counter</li>
	 *                <li>6-31 = in compatible range</li>
	 *                <li>32 - 63 = in private range</li>
	 *                </ul>
	 */
	public void setRequest(CounterInterrogationCommandRequest request) {
		this.request = request;
	}

	/**
	 * Returns the freeze value.
	 * 
	 * @return An enum entry in:
	 *         <ul>
	 *         <li>{@link Freeze#READ}</li>
	 *         <li>{@link Freeze#COUNTER_FREEZE_WITHOUT_RESET}</li>
	 *         <li>{@link Freeze#COUNTER_FREEZE_WITH_RESET}</li>
	 *         <li>{@link Freeze#COUNTER_RESET}</li>
	 *         </ul>
	 */
	public Freeze getFreeze() {
		return freeze;
	}

	/**
	 * Sets the freeze value.
	 * 
	 * @param freeze An enum entry in:
	 *               <ul>
	 *               <li>{@link Freeze#READ}</li>
	 *               <li>{@link Freeze#COUNTER_FREEZE_WITHOUT_RESET}</li>
	 *               <li>{@link Freeze#COUNTER_FREEZE_WITH_RESET}</li>
	 *               <li>{@link Freeze#COUNTER_RESET}</li>
	 *               </ul>
	 */
	public void setFreeze(Freeze freeze) {
		this.freeze = freeze;
	}

	/**
	 * Empty default constructor.
	 */
	public CounterInterrogationCommandQualifier() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param request The counter interrogation command request:
	 *                <ul>
	 *                <li>0 = not used</li>
	 *                <li>-1, 1-4 = request counter group 1, 2, or 4. -1 for no
	 *                group</li>
	 *                <li>5 = general request counter</li>
	 *                <li>6-31 = in compatible range</li>
	 *                <li>32 - 63 = in private range</li>
	 *                </ul>
	 * @param freeze  The freeze value, an enum entry in:
	 *                <ul>
	 *                <li>{@link Freeze#READ}</li>
	 *                <li>{@link Freeze#COUNTER_FREEZE_WITHOUT_RESET}</li>
	 *                <li>{@link Freeze#COUNTER_FREEZE_WITH_RESET}</li>
	 *                <li>{@link Freeze#COUNTER_RESET}</li>
	 *                </ul>
	 */
	public CounterInterrogationCommandQualifier(CounterInterrogationCommandRequest request, Freeze freeze) {
		this.request = request;
		this.freeze = freeze;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((freeze == null) ? 0 : freeze.hashCode());
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CounterInterrogationCommandQualifier other = (CounterInterrogationCommandQualifier) obj;
		if (freeze != other.freeze) {
			return false;
		}
		if (request == null) {
			if (other.request != null) {
				return false;
			}
		} else if (!request.equals(other.request)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		request = new CounterInterrogationCommandRequest(byteAsInt & 0x3f);
		freeze = Freeze.getFreeze((byteAsInt & 0xc0) >> 6)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
						byteAsInt + " is not a valid counter interrogation command qualifier!"));
		return Arrays.copyOfRange(bytes, getEncodedSize(), bytes.length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		return new byte[] { (byte) (request.getValue() | (freeze.getCode() << 6)) };
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}