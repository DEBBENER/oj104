/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.APCI;
import de.uniol.inf.ei.oj104.model.APDU;
import de.uniol.inf.ei.oj104.model.ASDU;
import de.uniol.inf.ei.oj104.model.ASDUAddress;
import de.uniol.inf.ei.oj104.model.ASDUType;
import de.uniol.inf.ei.oj104.model.CauseOfTransmission;
import de.uniol.inf.ei.oj104.model.Confirm;
import de.uniol.inf.ei.oj104.model.DataUnitIdentifier;
import de.uniol.inf.ei.oj104.model.DataUnitType;
import de.uniol.inf.ei.oj104.model.SingleInformationObject;
import de.uniol.inf.ei.oj104.model.StructureQualifier;
import de.uniol.inf.ei.oj104.model.controlfield.InformationTransfer;
import de.uniol.inf.ei.oj104.model.informationelement.NormalizedValue;
import de.uniol.inf.ei.oj104.model.informationelement.QualityDescriptorWithOV;
import de.uniol.inf.ei.oj104.model.timetag.SevenOctetBinaryTime;

/**
 * Class that contains test for {@link APDU}.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class APDUTest {

	/**
	 * Test of creation of an {@link APDU} from bytes. Checking of the created
	 * fields in {@link APCI} and {@link ASDU}.s
	 */
	@Test
	public void testSet1APDU1() {
		byte[] bytes = new byte[] { 0x68, 0x17, 0x16, 0x08, 0x68, 0x00, 0x22, 0x01, 0x03, 0x00, (byte) 0xbf, 0x50, 0x04,
				0x07, 0x01, 0x21, (byte) 0xf9, 0x00, 0x04, (byte) 0xe2, 0x01, (byte) 0x88, (byte) 0xa7, 0x04, 0x11 };

		APDU apdu = new APDU();
		try {
			apdu.fromBytes(bytes);
		} catch (IEC608705104ProtocolException e) {
			fail(e.getMessage());
		}

		APCI apci = apdu.getApci();
		ASDU asdu = apdu.getAsdu();
		DataUnitIdentifier dataUnitIdentifier = asdu.getDataUnitIdentifier();
		DataUnitType dataUnitType = dataUnitIdentifier.getDataUnitType();
		SingleInformationObject io = (SingleInformationObject) asdu.getInformationObjects().get(0);
		NormalizedValue nva = (NormalizedValue) io.getInformationElements().get(0);
		QualityDescriptorWithOV qds = (QualityDescriptorWithOV) io.getInformationElements().get(1);
		SevenOctetBinaryTime tt = (SevenOctetBinaryTime) io.getTimeTag().get();

		assertEquals(23, apci.getLength());
		assertEquals(1035, ((InformationTransfer) apci.getControlField()).getSendSequenceNumber());
		assertEquals(52, ((InformationTransfer) apci.getControlField()).getReceiveSequenceNumber());
		assertEquals(new ASDUType(34, "measured value, normalized value with time tag CP56Time2a", "M_ME_TD_1"),
				dataUnitType.getTypeIdentification());
		assertEquals(StructureQualifier.SINGLE, dataUnitType.getStructureQualifier());
		assertEquals(1, dataUnitType.getNumberOfInformationElements());
		assertEquals(new CauseOfTransmission(3, "spontaneous"), dataUnitIdentifier.getCauseOfTransmission());
		assertEquals(Confirm.POSITIVE, dataUnitIdentifier.getConfirm());
		assertEquals(false, dataUnitIdentifier.isTest());
		assertEquals(0, dataUnitIdentifier.getOriginatorAddress());
		assertEquals(new ASDUAddress(20671), dataUnitIdentifier.getCommonAddressOfASDU());
		assertEquals(1, asdu.getInformationObjects().size());
		assertEquals(67332, io.getInformationObjectAddress());
		assertEquals(2, io.getInformationElements().size());
		assertEquals(-1759, nva.getValue(), 0.0);
		assertFalse(qds.isOverflow());
		assertFalse(qds.isBlocked());
		assertFalse(qds.isSubstituted());
		assertFalse(qds.isNotTopical());
		assertFalse(qds.isInvalid());
		assertEquals(new SevenOctetBinaryTime(860, 57, 1, false, false, 8, 7, 5, 4, 17), tt);
	}

}