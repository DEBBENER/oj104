/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.controlfield.InformationTransfer;
import de.uniol.inf.ei.oj104.model.controlfield.NumberedSupervisoryFunction;
import de.uniol.inf.ei.oj104.model.controlfield.UnnumberedControlFunction;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * APCI stands for application protocol control information. It is, so to say,
 * the header of an {@link APDU}. All APCIs (in binary form) must start with the
 * same start tag, contain the length of the rest of the {@link APDU} (length
 * minus start tag and length information), and a control field.
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class APCI implements IByteEncodedEntity {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -9045613670469433165L;

	/**
	 * The start tag (0x68) for all {@link APDU}s in binary form.
	 */
	private static final int startTag = 0x68;

	/**
	 * The maximum rest length for an {@link APDU} in binary form.
	 */
	private static final int maxLength = 253;

	/**
	 * The rest length (length minus start tag and length information) of the
	 * {@link APDU} in binary form.
	 */
	private int length;

	/**
	 * The control field, that defines the type of the {@link APDU} (I format, S
	 * format, U format; see 104 standard).
	 */
	private IControlField controlField;

	/**
	 * Return the rest length of the {@link APDU}.
	 * 
	 * @return The rest length (length minus start tag and length information) of
	 *         the {@link APDU} in binary form.
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Sets the rest length of the {@link APDU}.
	 * 
	 * @param length The rest length (length minus start tag and length information)
	 *               of the {@link APDU} in binary form. Max = 253
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * Returns the control field.
	 * 
	 * @return The control field, that defines the type of the {@link APDU} (I
	 *         format, S format, U format; see 104 standard).
	 */
	public IControlField getControlField() {
		return controlField;
	}

	/**
	 * Sets the control field.
	 * 
	 * @param controlField The control field, that defines the type of the
	 *                     {@link APDU} (I format, S format, U format; see 104
	 *                     standard).
	 */
	public void setControlField(IControlField controlField) {
		this.controlField = controlField;
	}

	/**
	 * Empty default constructor.
	 */
	public APCI() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param length       The rest length (length minus start tag and length
	 *                     information) of the {@link APDU} in binary form. Max =
	 *                     253
	 * @param controlField The control field, that defines the type of the
	 *                     {@link APDU} (I format, S format, U format; see 104
	 *                     standard).
	 */
	public APCI(int length, IControlField controlField) {
		this.length = length;
		this.controlField = controlField;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((controlField == null) ? 0 : controlField.hashCode());
		result = prime * result + length;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		APCI other = (APCI) obj;
		if (controlField == null) {
			if (other.controlField != null) {
				return false;
			}
		} else if (!controlField.equals(other.controlField)) {
			return false;
		}
		if (length != other.length) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		} else if ((bytes[0]) != startTag) {
			throw new IEC608705104ProtocolException(getClass(), "APCI must start with 68H!");
		} else if ((length = bytes[1] & 0xff) < IControlField.getEncodedSize() || length > maxLength) {
			throw new IEC608705104ProtocolException(getClass(), "Illegal APDU length! Length must be in ["
					+ IControlField.getEncodedSize() + ", " + maxLength + "]!");
		}

		int controlFieldHeader = bytes[2] & 0xff;
		if ((controlFieldHeader & 1) == 0) {
			controlField = new InformationTransfer();
		} else if ((controlFieldHeader & 3) == 1) {
			controlField = new NumberedSupervisoryFunction();
		} else if ((controlFieldHeader & 3) == 3) {
			controlField = new UnnumberedControlFunction();
		} else {
			throw new IEC608705104ProtocolException(getClass(),
					"Illegal first control field octet: " + controlFieldHeader);
		}
		controlField.fromBytes(Arrays.copyOfRange(bytes, 2, getEncodedSize()));
		// The length of the APDU is the amount of bytes after the length byte
		return Arrays.copyOfRange(bytes, getEncodedSize(), getEncodedSize() - IControlField.getEncodedSize() + length);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] headerBytes = new byte[] { startTag, (byte) (length & 0xff) };
		byte[] controLFieldBytes = controlField.toBytes();
		return ArrayUtils.addAll(headerBytes, controLFieldBytes);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 6;
	}

}