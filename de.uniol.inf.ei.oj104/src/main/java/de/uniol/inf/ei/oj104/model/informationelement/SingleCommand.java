/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a command with one possible value: on.
 * <p>
 * Abbreviation: SCO
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class SingleCommand implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = -615321935545988049L;

	// SCS
	/**
	 * True for on, false for off.
	 * <p>
	 * Abbreviation: SCS
	 */
	private boolean on;

	/**
	 * The command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * <p>
	 * Can have the following values:
	 * <ul>
	 * <li>0 = no additional information</li>
	 * <li>1 = short pulse duration</li>
	 * <li>2 = long pulse duration</li>
	 * <li>3 = persistent output</li>
	 * <li>4-8 = in compatible range</li>
	 * <li>9-15 = reserved for other predefined functions</li>
	 * <li>16-31 = in private range</li>
	 * </ul>
	 */
	private CommandQualifier qualifier;

	/**
	 * Checks, whether the command means on.
	 * <p>
	 * Abbreviation: SCS
	 * 
	 * @return True for on, false for off.
	 */
	public boolean isOn() {
		return on;
	}

	/**
	 * Sets, whether the command means on.
	 * <p>
	 * Abbreviation: SCS
	 * 
	 * @param on True for on, false for off.
	 */
	public void setOn(boolean on) {
		this.on = on;
	}

	/**
	 * Returns the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @return An integer in:
	 *         <ul>
	 *         <li>0 = no additional information</li>
	 *         <li>1 = short pulse duration</li>
	 *         <li>2 = long pulse duration</li>
	 *         <li>3 = persistent output</li>
	 *         <li>4-8 = in compatible range</li>
	 *         <li>9-15 = reserved for other predefined functions</li>
	 *         <li>16-31 = in private range</li>
	 *         </ul>
	 */
	public CommandQualifier getQualifier() {
		return qualifier;
	}

	/**
	 * Sets the command qualifier.
	 * <p>
	 * Abbreviation: QOC
	 * 
	 * @param qualifier An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range</li>
	 *                  </ul>
	 */
	public void setQualifier(CommandQualifier qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * Empty default constructor.
	 */
	public SingleCommand() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param command   The command.
	 *                  <p>
	 *                  Abbreviation: SCS
	 *                  <p>
	 *                  True for on, false for off.
	 * @param qualifier The command qualifier.
	 *                  <p>
	 *                  Abbreviation: QOC
	 *                  <p>
	 *                  An integer in:
	 *                  <ul>
	 *                  <li>0 = no additional information</li>
	 *                  <li>1 = short pulse duration</li>
	 *                  <li>2 = long pulse duration</li>
	 *                  <li>3 = persistent output</li>
	 *                  <li>4-8 = in compatible range</li>
	 *                  <li>9-15 = reserved for other predefined functions</li>
	 *                  <li>16-31 = in private range
	 */
	public SingleCommand(boolean on, CommandQualifier qualifier) {
		this.on = on;
		this.qualifier = qualifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (on ? 1231 : 1237);
		result = prime * result + ((qualifier == null) ? 0 : qualifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SingleCommand other = (SingleCommand) obj;
		if (on != other.on) {
			return false;
		}
		if (qualifier == null) {
			if (other.qualifier != null) {
				return false;
			}
		} else if (!qualifier.equals(other.qualifier)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;
		on = (byteAsInt & 0x01) == 0x01;
		qualifier = new CommandQualifier();
		byte[] remainingBytes = qualifier.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualifier.toBytes();
		qdsBytes[0] |= (on ? 0x01 : 0x00);
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return Byte.BYTES;
	}

}