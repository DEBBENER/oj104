/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.uniol.inf.ei.oj104.model.CauseOfTransmission;

/**
 * Util class that loads all information about {@link CauseOfTransmission} from
 * /META-INF/CausesOfTransmission.xml and that provides several getters. A
 * single {@link CauseOfTransmission} entry has a cause tag and the following
 * attributes:
 * <ul>
 * <li>id: The consecutive number used in the IEC 60870-5-101 standard.</li>
 * <li>description: The short description used in the IEC 60870-5-101 standard,
 * e.g. "periodic, cyclic"</li>
 * </ul>
 * It is also possible to define a range of equal {@link CauseOfTransmission},
 * e.g. for a private range. That can be done with an cause_range tag that has
 * the following attributes:
 * <ul>
 * <li>first_id: The first consecutive number used in the IEC 60870-5-101
 * standard.</li>
 * <li>last_id: The last consecutive number used in the IEC 60870-5-101
 * standard.</li>
 * <li>description: The short description used in the IEC 60870-5-101 standard,
 * e.g. "reserved for further compatible definitions"</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class CauseOfTransmissionFactory {

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = LoggerFactory.getLogger(CauseOfTransmissionFactory.class);

	/**
	 * The XML file containing the information about the causes of transmissions.
	 */
	private static final String xmlFile = "/META-INF/CausesOfTransmission.xml";

	/**
	 * The XML tag of a single cause of transmission entry in {@link #xmlFile}.
	 */
	private static final String causeTag = "cause";

	/**
	 * The XML tag of a causes of transmission range entry, e.g. "reserved for
	 * further compatible definitions" in {@link #xmlFile}.
	 */
	private static final String causeRangeTag = "cause_range";

	/**
	 * The key of the ID attribute in an {@link #causeTag}. The IDs are subsequent
	 * numbers defined in the standard.
	 */
	private static final String idAttribute = "id";

	/**
	 * The key of the first ID attribute in an {@link #causeRangeTag}. The IDs are
	 * subsequent numbers defined in the standard.
	 */
	private static final String firstIdAttribute = "first_id";

	/**
	 * The key of the last ID attribute in an {@link #causeRangeTag}. The IDs are
	 * subsequent numbers defined in the standard.
	 */
	private static final String lastIdAttribute = "last_id";

	/**
	 * The key of the description attribute in an {@link #causeTag} or
	 * {@link #causeRangeTag}. The descriptions are human readable and defined in
	 * the standard.
	 */
	private static final String descriptionAttribute = "description";

	/**
	 * Mapping of all {@link CauseOfTransmission}s, read from XML, to their IDs.
	 */
	private static Map<Integer, CauseOfTransmission> causes = new HashMap<>();

	static {
		init();
	}

	/**
	 * Loads everything from XML file.
	 * <p>
	 * Calls {@link #loadFromCauseElements(Document)} and
	 * {@link #loadFromCauseRangeElements(Document)}.
	 */
	private static void init() {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try (InputStream in = CauseOfTransmissionFactory.class.getResourceAsStream(xmlFile)) {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(in);
			doc.getDocumentElement().normalize();
			loadFromCauseElements(doc);
			loadFromCauseRangeElements(doc);
		} catch (SAXException | ParserConfigurationException | IOException e1) {
			logger.error("Error while parsing CausesOfTransmission.xml");
		}
	}

	/**
	 * Loads all single {@link CauseOfTransmission} entries from an XML document.
	 * 
	 * @param doc The XML document.
	 */
	private static void loadFromCauseElements(Document doc) {
		NodeList nodeList = doc.getElementsByTagName(causeTag);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				CauseOfTransmission cause = new CauseOfTransmission(Integer.parseInt(element.getAttribute(idAttribute)),
						element.getAttribute(descriptionAttribute));
				causes.put(cause.getId(), cause);
			}
		}
	}

	/**
	 * Loads all {@link CauseOfTransmission} range entries from an XML document.
	 * 
	 * @param doc The XML document.
	 */
	private static void loadFromCauseRangeElements(Document doc) {
		NodeList nodeList = doc.getElementsByTagName(causeRangeTag);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				for (int id = Integer.parseInt(element.getAttribute(firstIdAttribute)); id <= Integer
						.parseInt(element.getAttribute(lastIdAttribute)); id++) {
					CauseOfTransmission cause = new CauseOfTransmission(id, element.getAttribute(descriptionAttribute));
					causes.put(cause.getId(), cause);
				}
			}
		}
	}

	/**
	 * Get all stored {@link CauseOfTransmission}.
	 */
	public static Collection<CauseOfTransmission> getAllCausesOfTransmission() {
		return causes.values();
	}

	/**
	 * Get the {@link CauseOfTransmission} with a given id if present.
	 */
	public static Optional<CauseOfTransmission> getCauseOfTransmission(int id) {
		return Optional.ofNullable(causes.get(id));
	}

	/**
	 * Get the ids of all stored {@link CauseOfTransmission}s.
	 */
	public static Set<Integer> getAllCausesOfTransmissionIds() {
		return causes.keySet();
	}

}