/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import org.apache.commons.lang3.ArrayUtils;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a binary counter reading.
 * <p>
 * Abbreviation: BCR
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class BinaryCounterReading extends IntegerValue implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 7446139928275704926L;

	/**
	 * The sequence notation: sequence number, carry flag (y/n), counter adjusted
	 * (y/n), and invalid (y/n).
	 */
	private SequenceNotation sequenceNotation;

	/**
	 * Returns the sequence notation.
	 * 
	 * @return A sequence notation contains a sequence number, carry flag (y/n),
	 *         counter adjusted (y/n), and invalid (y/n).
	 */
	public SequenceNotation getSequenceNotation() {
		return sequenceNotation;
	}

	/**
	 * Sets the sequence notation.
	 * 
	 * @param sequenceNotation A sequence notation contains a sequence number, carry
	 *                         flag (y/n), counter adjusted (y/n), and invalid
	 *                         (y/n).
	 */
	public void setSequenceNotation(SequenceNotation sequenceNotation) {
		this.sequenceNotation = sequenceNotation;
	}

	/**
	 * Empty default constructor.
	 */
	public BinaryCounterReading() {
		super();
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value            The binary counter reading.
	 * @param sequenceNotation A sequence notation contains a sequence number, carry
	 *                         flag (y/n), counter adjusted (y/n), and invalid
	 *                         (y/n).
	 */
	public BinaryCounterReading(int value, SequenceNotation sequenceNotation) {
		super(value);
		this.sequenceNotation = sequenceNotation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((sequenceNotation == null) ? 0 : sequenceNotation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BinaryCounterReading other = (BinaryCounterReading) obj;
		if (sequenceNotation == null) {
			if (other.sequenceNotation != null) {
				return false;
			}
		} else if (!sequenceNotation.equals(other.sequenceNotation)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		byte[] remainingBytes = super.fromBytes(bytes);
		sequenceNotation = new SequenceNotation();
		return sequenceNotation.fromBytes(remainingBytes);
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] valueBytes = super.toBytes();
		byte[] sqBytes = sequenceNotation.toBytes();
		return ArrayUtils.addAll(valueBytes, sqBytes);
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return IntegerValue.getEncodedSize() + SequenceNotation.getEncodedSize();
	}

}