/*******************************************************************************
 * Copyright 2018 University of Oldenburg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package de.uniol.inf.ei.oj104.model.informationelement;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Optional;

import de.uniol.inf.ei.oj104.exception.IEC608705104ProtocolException;
import de.uniol.inf.ei.oj104.model.IInformationElement;
import de.uniol.inf.ei.oj104.util.JsonParser;

/**
 * Information element that contains a set point with three possible values.
 * <p>
 * Abbreviation: DPI
 * <p>
 * <ul>
 * <li>not determinable</li>
 * <li>off</li>
 * <li>on</li>
 * </ul>
 * 
 * @author Michael Brand (michael.brand@uol.de)
 *
 */
public class DoublePointInformation implements IInformationElement {

	/**
	 * The version of this class for serialization.
	 */
	private static final long serialVersionUID = 217242263904537633L;

	/**
	 * Enumeration for double point values.
	 * 
	 * @author Michael Brand (michael.brand@uol.de)
	 *
	 */
	public enum DoublePointInformationEnum implements Serializable {

		/**
		 * Not determinable.
		 */
		INDETERMINATE_0(0),

		/**
		 * Off.
		 */
		OFF(1),

		/**
		 * On.
		 */
		ON(2),

		/**
		 * Not permitted.
		 */
		INDETERMINATE_3(3);

		/**
		 * The code of the set point:
		 * <ul>
		 * <li>0, 3 = indeterminable</li>
		 * <li>1 = off</li>
		 * <li>2 = on</li>
		 * </ul>
		 */
		private int code;

		/**
		 * Returns the code of the set point.
		 * 
		 * @return An integer in:
		 *         <ul>
		 *         <li>0, 3 = indeterminable</li>
		 *         <li>1 = off</li>
		 *         <li>2 = on</li>
		 *         </ul>
		 */
		public int getCode() {
			return code;
		}

		/**
		 * Constructor with fields.
		 * 
		 * @param code An integer in:
		 *             <ul>
		 *             <li>0, 3 = indeterminable</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 */
		private DoublePointInformationEnum(int code) {
			this.code = code;
		}

		/**
		 * Retrieve an enum entry.
		 * 
		 * @param code The code of the set point:
		 *             <ul>
		 *             <li>0, 3 = indeterminable</li>
		 *             <li>1 = off</li>
		 *             <li>2 = on</li>
		 *             </ul>
		 * @return An optional of the {@link DoublePointInformationEnum} or
		 *         {@link Optional#empty()}, if there is none for the code.
		 */
		public static Optional<DoublePointInformationEnum> getInfo(int code) {
			return Arrays.asList(values()).stream().filter(dpi -> dpi.getCode() == code).findAny();
		}
	}

	/**
	 * The set point.
	 * <p>
	 * Abbreviation: DPI
	 * <p>
	 * <ul>
	 * <li>{@link DoublePointInformationEnum#INDETERMINATE_0}</li>
	 * <li>{@link DoublePointInformationEnum#OFF}</li>
	 * <li>{@link DoublePointInformationEnum#ON}</li>
	 * <li>{@link DoublePointInformationEnum#INDETERMINATE_3}</li>
	 * </ul>
	 */
	private DoublePointInformationEnum value;

	/**
	 * The quality descriptor for the set point. Can be blocked, substituted, not
	 * topical, or invalid.
	 */
	private QualityDescriptor qualityDescriptor = new QualityDescriptor();

	/**
	 * Returns the set point.
	 * <p>
	 * Abbreviation: DPI
	 * 
	 * @return One of the following values:
	 *         <ul>
	 *         <li>{@link DoublePointInformationEnum#INDETERMINATE_0}</li>
	 *         <li>{@link DoublePointInformationEnum#OFF}</li>
	 *         <li>{@link DoublePointInformationEnum#ON}</li>
	 *         <li>{@link DoublePointInformationEnum#INDETERMINATE_3}</li>
	 *         </ul>
	 */
	public DoublePointInformationEnum getValue() {
		return value;
	}

	/**
	 * Sets the set point.
	 * <p>
	 * Abbreviation: DPI
	 * 
	 * @param value One of the following values:
	 *              <ul>
	 *              <li>{@link DoublePointInformationEnum#INDETERMINATE_0}</li>
	 *              <li>{@link DoublePointInformationEnum#OFF}</li>
	 *              <li>{@link DoublePointInformationEnum#ON}</li>
	 *              <li>{@link DoublePointInformationEnum#INDETERMINATE_3}</li>
	 *              </ul>
	 */
	public void setValue(DoublePointInformationEnum value) {
		this.value = value;
	}

	/**
	 * Checks, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @return True, if the value is marked as blocked.
	 */
	public boolean isBlocked() {
		return qualityDescriptor.isBlocked();
	}

	/**
	 * Sets, whether the value is blocked.
	 * <p>
	 * Abbreviation: BL
	 * 
	 * @param blocked True, if the value shall be marked as blocked.
	 */
	public void setBlocked(boolean blocked) {
		qualityDescriptor.setBlocked(blocked);
	}

	/**
	 * Checks, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @return True, if the value is marked as substituted.
	 */
	public boolean isSubstituted() {
		return qualityDescriptor.isSubstituted();
	}

	/**
	 * Sets, whether the value is substituted.
	 * <p>
	 * Abbreviation: SB
	 * 
	 * @param substituted True, if the value shall be marked as substituted.
	 */
	public void setSubstituted(boolean substituted) {
		qualityDescriptor.setSubstituted(substituted);
	}

	/**
	 * Checks, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @return True, if the value is marked as not topical.
	 */
	public boolean isNotTopical() {
		return qualityDescriptor.isNotTopical();
	}

	/**
	 * Sets, whether the value is not topical.
	 * <p>
	 * Abbreviation: NT
	 * 
	 * @param notTopical True, if the value shall be marked as not topical.
	 */
	public void setNotTopical(boolean notTopical) {
		qualityDescriptor.setNotTopical(notTopical);
	}

	/**
	 * Checks, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @return True, if the value is marked as invalid.
	 */
	public boolean isInvalid() {
		return qualityDescriptor.isInvalid();
	}

	/**
	 * Sets, whether the value is invalid.
	 * <p>
	 * Abbreviation: IV
	 * 
	 * @param invalid True, if the value shall be marked as invalid.
	 */
	public void setInvalid(boolean invalid) {
		qualityDescriptor.setInvalid(invalid);
	}

	/**
	 * Empty default constructor.
	 */
	public DoublePointInformation() {
	}

	/**
	 * Constructor with fields.
	 * 
	 * @param value             The set point (DPI) can be:
	 *                          <ul>
	 *                          <li>{@link DoublePointInformationEnum#INDETERMINATE_0}</li>
	 *                          <li>{@link DoublePointInformationEnum#OFF}</li>
	 *                          <li>{@link DoublePointInformationEnum#ON}</li>
	 *                          <li>{@link DoublePointInformationEnum#INDETERMINATE_3}</li>
	 *                          </ul>
	 * @param qualityDescriptor The quality descriptor for the set point. Can be
	 *                          blocked, substituted, not topical, or invalid.
	 */
	public DoublePointInformation(DoublePointInformationEnum value, QualityDescriptor qualityDescriptor) {
		this.value = value;
		this.qualityDescriptor = qualityDescriptor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((qualityDescriptor == null) ? 0 : qualityDescriptor.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DoublePointInformation other = (DoublePointInformation) obj;
		if (qualityDescriptor == null) {
			if (other.qualityDescriptor != null) {
				return false;
			}
		} else if (!qualityDescriptor.equals(other.qualityDescriptor)) {
			return false;
		}
		if (value != other.value) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return JsonParser.serialize(this);
	}

	@Override
	public byte[] fromBytes(byte[] bytes) throws IEC608705104ProtocolException {
		if (bytes.length < getEncodedSize()) {
			throw new IEC608705104ProtocolException(getClass(), getEncodedSize(), bytes.length);
		}

		int byteAsInt = bytes[0] & 0xff;

		value = DoublePointInformationEnum.getInfo(byteAsInt & 0x03)
				.orElseThrow(() -> new IEC608705104ProtocolException(getClass(),
						byteAsInt + " is not a valid double point information!"));
		qualityDescriptor = new QualityDescriptor();
		byte[] remainingBytes = qualityDescriptor.fromBytes(bytes);

		return remainingBytes;
	}

	@Override
	public byte[] toBytes() throws IEC608705104ProtocolException {
		byte[] qdsBytes = qualityDescriptor.toBytes();
		qdsBytes[0] |= value.getCode();
		return qdsBytes;
	}

	/**
	 * Returns the needed amount of bytes for binary serialization.
	 * 
	 * @return The needed amount of bytes for binary serialization.
	 */
	public static int getEncodedSize() {
		return 1;
	}

}